# Module SY40 : Industrie 4.0, traçabilité et blockchain


Liens utiles :

- [Support cours](docs/20190919 SY40 traçabilité blockchain.pdf)
- [Synoptique](http://api.utt.phec.net/synoptic/index.html)
- [ERP](http://erp.utt.phec.net)
- [API docs](http://api.utt.phec.net/docs) ou [redoc](http://api.utt.phec.net/redoc)

# Etudiants

2 groupes de 15 étudiants : demande pour avoir 3 groupes

- Si 2 groupes alors 2 creneaux sur la même semaine, toutes les 2 semaines
- Si 3 groupes alors 2 creneaux sur la même semaine et 1 créneau la semaine d'après

# Points en suspens

- Demande d'avoir 1 CM en début de module, pour présenter le TP dans sa globalité :
  (A 1 CM correspond 2 TDs)

- confirmer à Taha si 1 ou 2 CMs, au plus tard ce Vendredi ?
  Si 2 CMs alors le second porterait sur des notions de blockchain

- voir pour le réglement ?

- élaborer le cas d'usage d'ordonnancement en temps réel (Ludo, Taha)
  - exemple donné : cmd(5 cartons), cmd(10 cartons), cmd(8 cartons)
  - définir la politique d'ordonnancement : ordre décroissant sur les quantités, fifo, lifo, ...

# Projet de création du TP blockchain à l'UTT pour la rentrée septembre 2019.

## Objectif:
Mise en Blockchain d'une entreprise sur différents process du workflow ,en simulant une entreprise d'embouteillage de champagnes.
Faire comprendre aux étudiants l'intérêt d'une telle mise en place, dans la confiance et la traçabilité.
Montrer comment une entreprise non digitalisée peut évoluer vers une entreprise digitalisée.


## Evaluation:
Scénario consistant à donner une situation aux étudiants qui au début se déroulera sans prblème pour tester leur organisation, leur capacité à travailler en groupe et leurs compétences sur les process intégrant la blockchain.
Ensuite il se passera un problème (problème d'intoxication, de retard sur une commende...) pour tester leur réactivité et leur capacité à s'appuyer sur la technologie Blockchain pour gérer le conflit.




## TP
|:-------|:---------------|:-----------|
|TP N° 1 |Fonctionnalités | Survey Map |
|SI : ERP Seul ; Atelier manuel|informations commerciales et de gestion sous Dolibarr; Production/Livraison/Expédition/logistique sous google sheet, avec post-it | Survey Map |
|TP N° 1 |Fonctionnalités | Survey Map |
|TP N° 1 |Fonctionnalités | Survey Map |
|TP N° 1 |Fonctionnalités | Survey Map |
|TP N° 1 |Fonctionnalités | Survey Map |
|TP N° 1 |Fonctionnalités | Survey Map |
|TP N° 1 |Fonctionnalités | Survey Map |
|TP N° 1 |Fonctionnalités | Survey Map |
|TP N° 1 |Fonctionnalités | Survey Map |



Création de scénarios pour les différents TP.

### TP1: Atelier de production "manuel"

Objectif :
Comprendre les concepts de base d'une gestion de production

Déroulement proposé :
Prise en main de l'environnement Dollibar.
Chacun doit appréhender les différents postes non blockchainisés sur un process simple et un scénario où tout se passe bien sauf peut-être à la fin de la séance.

- 1h: chacun a un dolibarr dans lequel il doit créer les fournisseurs, les objets fournisseurs, les objets clients, les stocks et exécuter un process complet.

  - créer une commande client
  - créer les commandes fournisseurs
  - faire les mouvements de stocks
  - faire les arbitrage de stocks
  - expédier la commande

- 1h: On regroupe tous les étudiants sur un même Dolibarr déjà configuré ou chaque binôme aura un rôle qu'il devra s'attribuer. Gérer les échanges avec des post-it de couleur?? Echanges entre gestion et production gérés par google sheet sur le drive de l'adresse du groupe:
  - création des comptes utilisateurs et gestion des droits de chaque rôle
  - commercial qui saisit les commandes client (ou bien nous-même qui les envoyons via API ou via mail?)
  - commercial qui saisit les commandes fournisseurs
  - gestion des stocks
  - expédition des commandes client
  - vérification que tout se passe bien?

A la fin du TP1, Evaluation de la maturité digitale de l'entreprise 4.0, sur http://survey.chaire-connected-innovation.com/

Mise en place d'un incident en fin de séance (commande non arrivée, intoxication, ...)

### TP2: Atelier de production automatisé

Déroulement proposé :
Tous les services Dolibarr sont actifs sous Dolibarr.
Il n'y a pas de simulation
Mise en place d'une tournante des binômes sur les différents postes.

Réglage du problème de fin de séance 1 sans la technologie blockchain. Que mettre en place pour éviter ce problème? se protéger? Intérêt de la technologie Blockchain.
Automatisation du process de prise de commande.
Automatisation du process de production.
Introduction d'incidents dans le code python.

Objectif :

- Comprendre un système de production automatisé

Déroulement proposé :

- Tous les services Dolibarr sont actifs sous Dolibarr.
- Automatisation du process de prise de commande.
- Automatisation du process de production.
- Introduction d'incidents dans le code python.
- Mise en place d'une tournante des binômes sur les différents postes.

Réglage du problème de fin de séance 1 sans la technologie blockchain.
Que mettre en place pour éviter ce problème? se protéger? Intérêt de la technologie Blockchain.

### TP3 : Atelier de production simulé avec des évènements externes

Objectif :

- Optimiser les coûts de production.

Déroulement proposé :

- Tous les services Dolibarr sont actifs.
- Mise en place de la simulation de production, intégrant la gestion évènementielle.
- Mise en place d'une tournante des binômes sur les différents postes.


### TP4 : Atelier certifié conforme (vis à vis des réglementations de traçabilité, de RGPD, ...)

Objectif :
Garantir la conformité réglementaire

Déroulement proposé :
Tous les services Dolibarr sont actifs.
Toutes les fonctionnalités de simulation sont actives.
Rajout des composants blockchain pour certifier les flux (via les micro-controleurs)
Mise en place d'une tournante des binômes sur les différents postes.


Objectif :

- Garantir la conformité réglementaire

Déroulement proposé :

- Tous les services Dolibarr sont actifs.
- Toutes les fonctionnalités de simulation sont actives.
- Rajout des composants blockchain pour certifier les flux (via les micro-controleurs)
- Mise en place d'une tournante des binômes sur les différents postes.

### TP5 : Atelier de dossier de preuves

Objectif :

- Elaborer un dossier de preuves

Déroulement proposé

- Tous les services Dolibarr sont actifs.
- Toutes les fonctionnalités de simulation sont actives.
- Tous les composants blockchain sont actifs.
- Elaboration de journaux de transactions
- Mise en place d'une tournante des binômes sur les différents postes.

A la fin du TP5, Evaluation de la maturité digitale de l'entreprise 4.0, sur http://survey.chaire-connected-innovation.com/


## Initialisation de la plateforme ERP:

Accedez à config / Modules applications pour mettre à disposition les menus Dolibarr.

Sélectionnez les modules suivants :
* Tiers
* Commandes clients
* Expéditions
* Fournisseurs
* Factures et avoirs
* Produits
* Stocks


## à classer

[Simulation production](simprod/index.md)

### Dolibarr volumes

Les volumes mariadb et dolibarr document peuvent être vide.
Mais le volume dolibarr html doit être pré-rempli.

Pour récupérer le contenu initial de /var/www/documents
qui servira à peupler le volume correspondant.

Démarrer un nouveau dolibarr avec le docker-compose :

```yaml
mariadb:
  image: mariadb:latest
  environment:
    MYSQL_ROOT_PASSWORD: root
    MYSQL_DATABASE: dolibarr

web:
  image: tuxgasy/dolibarr
  environment:
    DOLI_DB_HOST: mariadb
    DOLI_DB_USER: root
    DOLI_DB_PASSWORD: root
    DOLI_DB_NAME: dolibarr
    DOLI_URL_ROOT: "http://0.0.0.0"
    PHP_INI_DATE_TIMEZONE: "Europe/Paris"
  ports:
    - "80:80"
  links:
    - mariadb
```



```
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
     echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list

sudo apt-get update && sudo apt-get install -y
  nodejs \
  python3-dev \
  yarn
```

```
yarn install

```

### Compiles and hot-reloads for development
```

yarn run serve

```

### Compiles and minifies for production
```

yarn run build

```

### Run your tests
```

yarn run test

```

### Lints and fixes files
```

yarn run lint

```

### Run your end-to-end tests
```

yarn run test:e2e

```

### Run your unit tests
```

yarn run test:unit

```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
