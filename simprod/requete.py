import requests
import json
import time
import datetime


HEADERS = {'DOLAPIKEY': 'rLc6LDR94MQ5cMTog80i8ioni7UDSv45'}
URL = 'http://erp.utt.phec.net/api/index.php'

#API_ENDPOINT = 'http://localhost:8000'
API_ENDPOINT = 'http://api.utt.phec.net'


def etat_stocks():
    req = requests.get(url=API_ENDPOINT + '/getstocks',headers = HEADERS)
    print(req.json())
    return req.json()

def etat_stock_bouteille(warehouse):
    req = requests.get(f'{URL}/stockmovements?sortorder=ASC&limit=1000000')
    bouteilles = 0
    for lab in req.json():
        if lab['product_id'] == '1' and lab['warehouse_id'] == warehouse:
            bouteilles += int(lab['qty'])
    return bouteilles

def etat_stock_vin(warehouse):
    req = requests.get(f'{URL}/stockmovements?sortorder=ASC&limit=1000000')
    vin = 0
    for lab in req.json():
        if lab['product_id'] == '2' and lab['warehouse_id'] == warehouse:
            vin += float(lab['qty'])
    return vin

def etat_stock(product_id, warehouse):
    
    req = requests.get(f'{URL}/stockmovements?sortorder=ASC&limit=1000000', headers = HEADERS)
    prod = 0
    try:
        for lab in req.json():
            if lab['product_id'] == product_id and lab['warehouse_id'] == warehouse:
                prod += float(lab['qty'])
    except:
        pass
    return prod

def transfert_stock_bouteille():
    qty = etat_stock_bouteille('1')
    if qty == 0:
        res = False
    else:
        if qty >= 24:
            qty = 24
        data = {
            "product_id": '1',
            "warehouse_id": 1,
            "qty": -qty,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
        req = req = requests.post(f'{URL}/stockmovements', headers = HEADERS, data = data)
        if req.status_code == 200:
            data = {
            "product_id": '1',
            "warehouse_id": 2,
            "qty": qty,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
            req = requests.post(f'{URL}/stockmovements', headers = HEADERS, data = data)
            res = True
        else:
            res = False
        
    return res,qty

def transfert_stock_vin():
    qty = etat_stock_vin('1')
    if qty == 0:
        res = False
    else:
        if qty >= 30:
            qty = 30
        data = {
            "product_id": '2',
            "warehouse_id": 1,
            "qty": -qty,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
        req = req = requests.post(f'{URL}/stockmovements', headers = HEADERS, data = data)
        if req.status_code == 200:
            data = {
            "product_id": '2',
            "warehouse_id": 2,
            "qty": qty,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
            req = requests.post(f'{URL}/stockmovements', headers = HEADERS, data = data)
            res = True
        else:
            res = False
        
    return (res, qty)

def destock_bouteille():
    data = {
            "product_id": '1',
            "warehouse_id": 2,
            "qty": -1,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
    req = requests.post(
                f'{URL}/stockmovements',
                 headers = HEADERS,
                  data = data
                  )

    data = {
            "product_id": '2',
            "warehouse_id": 2,
            "qty": -0.75,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
    req = requests.post(
                f'{URL}/stockmovements',
                 headers = HEADERS,
                  data = data
                  )
    data = {
            "product_id": '6',
            "warehouse_id": 2,
            "qty": 1,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
    req = requests.post(
                f'{URL}/stockmovements',
                 headers = HEADERS,
                  data = data
                  )

def etiquetage():
    data = {
            "product_id": '8',
            "warehouse_id": 2,
            "qty": -1,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
    req = requests.post(
                f'{URL}/stockmovements',
                 headers = HEADERS,
                  data = data
                  )

    data = {
            "product_id": '5',
            "warehouse_id": 2,
            "qty": -1,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
    req = requests.post(
                f'{URL}/stockmovements',
                 headers = HEADERS,
                  data = data
                  )
    data = {
            "product_id": '7',
            "warehouse_id": 2,
            "qty": 1,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
    req = requests.post(
                f'{URL}/stockmovements',
                 headers = HEADERS,
                  data = data
                  )
def bouchage():
    data = {
            "product_id": '6',
            "warehouse_id": 2,
            "qty": -1,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
    req = requests.post(
                f'{URL}/stockmovements',
                 headers = HEADERS,
                  data = data
                  )

    data = {
            "product_id": '3',
            "warehouse_id": 2,
            "qty": -1,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
    req = requests.post(
                f'{URL}/stockmovements',
                 headers = HEADERS,
                  data = data
                  )
    data = {
            "product_id": '8',
            "warehouse_id": 2,
            "qty": 1,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
    req = requests.post(
                f'{URL}/stockmovements',
                 headers = HEADERS,
                  data = data
                  )

def bouchons():
    qty = etat_stock('3','1')
    if qty == 0:
        res = False
    else:
        if qty >= 30:
            qty = 30
        data = {
            "product_id": '3',
            "warehouse_id": 1,
            "qty": -qty,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
        req = req = requests.post(f'{URL}/stockmovements', headers = HEADERS, data = data)
        if req.status_code == 200:
            data = {
            "product_id": '3',
            "warehouse_id": 2,
            "qty": qty,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
            req = requests.post(f'{URL}/stockmovements', headers = HEADERS, data = data)
            res = True
        else:
            res = False
        
    return res,qty


def transfert_stock_etiquette():
    qty = etat_stock('5','1')
    if qty == 0:
        res = False
    else:
        if qty >= 100:
            qty = 100
        data = {
            "product_id": '5',
            "warehouse_id": 1,
            "qty": -qty,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
        req = req = requests.post(f'{URL}/stockmovements', headers = HEADERS, data = data)
        if req.status_code == 200:
            data = {
            "product_id": '5',
            "warehouse_id": 2,
            "qty": qty,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
            req = requests.post(f'{URL}/stockmovements', headers = HEADERS, data = data)
            res = True
        else:
            res = False
        
    return res,qty


def empaquetage():
    data = {
            "product_id": '7',
            "warehouse_id": 2,
            "qty": -6,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
    req = requests.post(
                f'{URL}/stockmovements',
                 headers = HEADERS,
                  data = data
                  )

    data = {
            "product_id": '4',
            "warehouse_id": 2,
            "qty": 1,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
    req = requests.post(
                f'{URL}/stockmovements',
                 headers = HEADERS,
                  data = data
                  )

def destock(ide, qty):
    data = {
        'id':ide,
        'qty':qty
    }
    req = requests.post(url=API_ENDPOINT + f'/destock', json=data)


def destocks(QTYS):
    data = QTYS
    print("QTYS ", data)
    req = requests.post(url=API_ENDPOINT + f'/destocks', json=data)


def destocks_lots(QTYS):
    data = QTYS
    print("QTYS ", data)
    req = requests.post(url=API_ENDPOINT + f'/destocks_lots', json=data)

    



def reset():
    req = requests.get(url=API_ENDPOINT + '/reset')
    
    return 'reseted'

def lots_vin():
    req = requests.get(f'{URL}/stockmovements?sortorder=ASC&limit=10000', headers = HEADERS)
    VIN = 0
    LOTS = []
    repartition = {}
    for lab in req.json():
        if lab['product_id'] == '8' and lab['warehouse_id'] == '1':
            VIN += float(lab['qty'])
            lot = lab.get('batch')
            if lot:
                LOTS.append(lot)
                if repartition.get(lot):
                    repartition[lot] += float(lab['qty'])
                else:
                    repartition[lot] = float(lab['qty'])
            
    return repartition