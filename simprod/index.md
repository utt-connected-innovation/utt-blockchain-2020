### simulation production


Modèle de production simplifié :

```mermaid
graph LR;
    subgraph MP
        Vin;
        Bouteille;
        Bouchon;
        Etiquette;
    end
    subgraph PF
        Caisse;
    end
    Vin-->Remplissage;
    Bouteille-->Remplissage;
    Bouchon-->Encapsulage;
    Etiquette-->Etiquetage;
    Remplissage-->Encapsulage;
    Encapsulage-->Etiquetage;
    Etiquetage-->Empaquetage;
    Empaquetage-->Caisse;
```


Variante où l'on ajoute un deuxième ilot d'empaquetage :

```mermaid
graph LR;
    subgraph MP
        Vin;
        Bouteille;
        Bouchon;
        Etiquette;
    end
    subgraph PF
        Caisse;
    end
    Vin-->Remplissage;
    Bouteille-->Remplissage;
    Bouchon-->Encapsulage;
    Etiquette-->Etiquetage;
    Remplissage-->Encapsulage;
    Encapsulage-->Etiquetage;
    Etiquetage-->Empaquetage1;
    Etiquetage-->Empaquetage2;
    Empaquetage1-->Caisse;
    Empaquetage2-->Caisse;
```

Essai avec [simpy](https://simpy.readthedocs.io/en/latest/).
