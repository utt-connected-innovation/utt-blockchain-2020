#!/usr/bin/env python
import simpy
import random
import requete

REPAIR_TIME = 25
NUM_ENCAP = 6
ENCAP_TIME = 20
NUM_ETIQUE = 1
ETIQUE_TIME = 5
CONF = {"capacite_bouteille": 0.75}
MEAN_PANNE = 150
TIME = 200
NUM_AGENT = 3

def min_stocks_bouteille():
    stocks = [bouchage.stock_bouteille for bouchage in bouchages]
    ind_min = stocks.index(min(stocks))
    return ind_min

def min_stocks_bouteille_bouch():
    """
    détermine vers quelle étiqueteuse envoyer la bouteille sortant
    de l'encapsuleuse
    """
    stocks = [etique.bouteille_ne for etique in etiquetages]
    ind_min = stocks.index(min(stocks))
    return ind_min

def time_to_failure():
    """Return time until next failure for a machine."""
    return random.expovariate(1/MEAN_PANNE)



class Remplissage(object):
    def __init__(self, env, maintenance_agent):
        self.env = env
        # Start the run process everytime an instance is created.
        self.bouteille_remplie = 0
        self.action = env.process(self.run(maintenance_agent))
        self.stock_bouteille_vide = requete.etat_stock_bouteille('2')
        self.stock_vin = requete.etat_stock_vin('2')
        self.stock_bouteille_pleine = 0
        self.en_panne = False
        self.pannes = 0
        env.process(self.panne())

    def run(self, maintenance_agent):
        while True:
            if self.stock_bouteille_vide > 0  and self.stock_vin > 1:
                print("Remplissage démarré %d" % self.env.now)
                process_duration = 3
                # We yield the process that process() returns
                # to wait for it to finish
                try:
                    yield self.env.process(self.remplir(process_duration))
                # The charge process has finished and
                # we can start driving again.
                    requete.destock_bouteille()
                    print("Attente bouteille suivante at %d" % self.env.now)
                    trip_duration = 1
                    yield self.env.timeout(trip_duration)
                except:
                    if self.en_panne:
                        with maintenance_agent.request(priority=1) as req:
                            yield req
                            yield self.env.timeout(REPAIR_TIME)
                        self.en_panne = False
                        print(f'Remplissage réparée !!! at {self.env.now}')

            else:
                if self.stock_bouteille_vide == 0:
                    (stock_bouteille_suffisant, qty) = requete.transfert_stock_bouteille()
                    print(f'------------------> transfert des bouteilles : {self.stock_bouteille_vide}')
                    if stock_bouteille_suffisant:
                        print('en attente bouteilles sur remplissage...')
                        recharge_time = 10
                        while recharge_time:
                            try:
                                with maintenance_agent.request(priority=2) as req:
                                    yield req                        
                                    yield self.env.timeout(recharge_time)
                                    self.stock_bouteille_vide += qty                        
                                    recharge_time = 0
                            except simpy.Interrupt:
                                if self.en_panne:
                                    recharge_time = 4  
                                    print(f'Remplissage: ',recharge_time)
                                    with maintenance_agent.request(priority=1) as req:
                                        yield req
                                        yield self.env.timeout(REPAIR_TIME)
                                    self.en_panne = False
                                    print(f'Remplissage réparée !!! at {self.env.now}')
                    else:
                        print('il faut commander des bouteilles!!!!!')
                        try:
                            yield self.env.timeout(10)
                        except simpy.Interrupt:
                            if self.en_panne:
                                  
                                with maintenance_agent.request(priority=1) as req:
                                    yield req
                                    yield self.env.timeout(REPAIR_TIME)
                                self.en_panne = False
                                print(f'Remplissage réparée !!! at {self.env.now}')


                    
                if self.stock_vin <= 1:
                    (stock_vin_suffisant, qty) = requete.transfert_stock_vin()
                    if stock_vin_suffisant:
                        print('en attente vin sur remplissage...')
                        recharge_time = 10
                        while recharge_time:
                            try:
                                with maintenance_agent.request(priority=2) as req:
                                    yield req                        
                                    yield self.env.timeout(recharge_time)
                                    self.stock_vin += qty                          
                                    recharge_time = 0
                            except simpy.Interrupt:
                                if self.en_panne:
                                    recharge_time = 4  
                                    print(f'Remplissage: ',recharge_time)
                                    with maintenance_agent.request(priority=1) as req:
                                        yield req
                                        yield self.env.timeout(REPAIR_TIME)
                                    self.en_panne = False
                                    print(f'Remplissage réparée !!! at {self.env.now}')
                    else:
                        print('il faut commander du vin!!!!!')
                        try:
                            yield self.env.timeout(10)
                        except simpy.Interrupt:
                            if self.en_panne:
                                  
                                with maintenance_agent.request(priority=1) as req:
                                    yield req
                                    yield self.env.timeout(REPAIR_TIME)
                                self.en_panne = False
                                print(f'Remplissage réparée !!! at {self.env.now}')
                    


    def panne(self):
        """la machine peut tomber en panne..."""
        while True:
            if self.en_panne:
                yield self.env.timeout(10)
            else:
                time_fail = time_to_failure()
                print(f'Remplissage : en panne dans {time_fail}')
                yield self.env.timeout(2*int(time_fail))
                if not self.en_panne:
                    self.en_panne = True  
                    self.pannes += 1                 
                    self.action.interrupt()


    def remplir(self, duration):
        self.bouteille_remplie += 1
        self.stock_bouteille_vide -= 1
        self.stock_vin -= CONF["capacite_bouteille"]
        yield self.env.timeout(duration)
        # self.stock_bouteille_pleine += 1
        # bouchage.stock_bouteille += 1
        bouch = bouchages[min_stocks_bouteille()]
        bouch.stock_bouteille += 1
        print("bouteilles remplies:",bouch.name)
        print(
            f"Bouteille remplie (stock={self.stock_bouteille_vide},{self.stock_vin},{self.stock_bouteille_pleine})"
        )
        


class Encapsulage(object):
    def __init__(self, env, name, maintenance_agent):
        self.env = env
        # Start the run process everytime an instance is created.
        self.name = name
        self.stock_bouteille = 0
        self.stock_bouchon = 0
        self.stock_bouteille_bouchee = 0
        self.action = env.process(self.run(maintenance_agent))
        self.en_attente = False
        self.en_panne = False
        self.recharge_time = 0
        self.start_recharge = 0
        self.pannes = 0
        self.envoi = 0
        self.bouchee = False
        
        env.process(self.panne())

    def run(self,maintenance_agent):
        while True:
            if self.stock_bouteille > 0 and self.stock_bouchon > 0:
            
                print(
                f"stock {self.name}={self.stock_bouteille},{self.stock_bouchon},{self.stock_bouteille_bouchee}"
            )
                print(f"{self.name}: Encapsulage démarré %d" % self.env.now)
                process_duration = ENCAP_TIME
                self.bouchee = False
                envoyee = False
                while not envoyee:
                    # We yield the process that process() returns
                    # to wait for it to finish
                    try:
                        if not self.bouchee:
                            start_encap = self.env.now                            
                            yield self.env.timeout(process_duration)
                            self.stock_bouteille_bouchee += 1                            
                            self.stock_bouteille -= 1
                            self.stock_bouchon -= 1
                            requete.bouchage()
                            self.bouchee = True
                            print(f"{self.name}:Attente bouteille suivante at %d" % self.env.now)
                            print(f"{self.name}: Envoi vers étiqueteuse")
                            yield self.env.timeout(2)
                            etiquetages[min_stocks_bouteille_bouch()].bouteille_ne += 1
                            self.envoi += 1                            
                            process_duration = 0
                            envoyee = True
                        else:
                            print(f"{self.name}:Attente bouteille suivante at {self.env.now}")
                            print(f"{self.name}: Envoi vers étiqueteuse")
                            yield self.env.timeout(2)
                            etiquetages[min_stocks_bouteille_bouch()].bouteille_ne += 1
                            self.envoi += 1
                            process_duration = 0 
                            envoyee = True
                    except simpy.Interrupt: 
                        if self.en_panne:                        
                            print(f'{self.name} en panne pendant bouchage, réparateur en route...')
                            process_duration -= self.env.now - start_encap

                            with maintenance_agent.request(priority=1) as req:
                                yield req
                                yield self.env.timeout(REPAIR_TIME)
                            self.en_panne = False
                            print(f'{self.name} réparée !!!')
                        

                """
                # The charge process has finished and
                # we can start driving again.
                print(f"{self.name}:Attente bouteille suivante at %d" % self.env.now)
                print(f"{self.name}: Envoi vers étiqueteuse")
                trip_duration = 2
                while trip_duration:
                    try:
                        start = self.env.now
                        yield self.env.timeout(trip_duration)
                        etiquetages[0].bouteille_ne += 1
                        trip_duration = 0
                    except simpy.Interrupt:                        
                        print(f'{self.name} en panne pendant attente, réparateur en route...')
                        trip_duration -= self.env.now - start
                        with maintenance_agent.request(priority=1) as req:
                            yield req
                            yield self.env.timeout(REPAIR_TIME)
                        self.en_panne = False
                        print(f'{self.name} réparée !!! at {self.env.now}')
                        
                 """       

                    
            else:
                if self.stock_bouchon == 0:
                    (stock_bouchons_suffisant,qty) = requete.bouchons()
                    if stock_bouchons_suffisant:
                        self.recharge_time = 4
                        while self.recharge_time:
                            try:
                                print(f'{self.name}: en attente bouchons')                                
                                self.start_recharge = self.env.now
                                #print(f' start at {self.start_recharge}', '1')
                                #print(f'during {self.recharge_time}', '1')                            
                                with maintenance_agent.request(priority=2) as req:
                                    yield req                        
                                    yield self.env.timeout(self.recharge_time)
                                    self.stock_bouchon += qty
                                                            
                                    self.recharge_time = 0  
                                                                                
                            except simpy.Interrupt:
                                if self.en_panne:
                                    print(f'{self.name} en panne pendant recharge bouchons, réparateur en route...{self.env.now}')
                                    print(f' start at {self.start_recharge}', '1')
                                    print(f'during {self.recharge_time}', '1')  
                                    #self.recharge_time -= (self.env.now - self.start_recharge)
                                    self.recharge_time = 4  
                                    print(f'{self.name}: ',self.recharge_time)
                                    with maintenance_agent.request(priority=1) as req:
                                        yield req
                                        yield self.env.timeout(REPAIR_TIME)
                                    self.en_panne = False
                                    print(f'{self.name} réparée !!! at {self.env.now}')
                                else:
                                    print(f'{self.name}: une tache plus importante est requise')
                                    # self.recharge_time -= (self.env.now - self.start_recharge)
                                    self.recharge_time = 4
                    else:
                        print('Il faut commander des bouchons!!!')
                        try:
                            yield self.env.timeout(10)
                        except simpy.Interrupt:
                            if self.en_panne:                                  
                                with maintenance_agent.request(priority=1) as req:
                                    yield req
                                    yield self.env.timeout(REPAIR_TIME)
                                self.en_panne = False
                                print(f'Bouchage réparée !!! at {self.env.now}')

                if self.stock_bouteille == 0:
                    wait_time = 3
                    while wait_time:
                        try:
                            print(f'{self.name}: en attente bouteille')
                            start_wait = self.env.now
                            yield self.env.timeout(wait_time)
                            wait_time = 0  # Set to 0 to exit while loop.
                        except simpy.Interrupt:
                            print(f'{self.name} en panne pendant bouteille vide, réparateur en route...')
                            wait_time -= self.env.now - start_wait
                            with maintenance_agent.request(priority=1) as req:
                                yield req
                                yield self.env.timeout(REPAIR_TIME)
                            self.en_panne = False
                            print(f'{self.name} réparée !!!')        
                
                            
                        

    def panne(self):
        """la machine peut tomber en panne..."""
        while True:
            if self.en_panne:
                yield self.env.timeout(10)
            else:
                time_fail = time_to_failure()
                print(f'{self.name} : en panne dans {time_fail}')
                yield self.env.timeout(int(time_fail))
                if not self.en_panne:
                    self.en_panne = True  
                    self.pannes += 1                   
                    self.action.interrupt()
                
            

    def bouchage(self, duration):
        if self.stock_bouteille > 0 and self.stock_bouchon > 0:
            
            yield self.env.timeout(duration)
            self.stock_bouteille_bouchee += 1
            #etiquetages[0].bouteille_ne += 1
            self.stock_bouteille -= 1
            self.stock_bouchon -= 1
            requete.bouchage()
            print(
                f"{self.name}: Bouteille bouchée (stock={self.stock_bouteille},{self.stock_bouchon},{self.stock_bouteille_bouchee} at {self.env.now})"
            )
        else:
            print(f"{self.name}: echec bouchage !!")

class Etiquetage(object):
    def __init__(self,env,name,maintenance_agent):
        self.env = env
        self.name = name
        self.bouteille_ne = 0
        self.etiquettes = 0
        self.bouteille_e = 0
        self.action = env.process(self.run(maintenance_agent))
        self.en_panne = False
        self.pannes = 0
        self.bouteille_etiquetees = 0
        env.process(self.panne())
        

    def run(self, maintenance_agent):
        while True:
            if self.bouteille_ne > 0 and self.etiquettes > 0:
                print(f'{self.name}: Etiquetage démarré at {self.env.now}')
                etique_duration = ETIQUE_TIME
                while etique_duration:
                    try:
                        start_etiqu = self.env.now
                        yield self.env.timeout(etique_duration)
                        etique_duration = 0
                        self.etiquettes -= 1
                        self.bouteille_ne -= 1
                        #self.bouteille_e += 1
                        self.bouteille_etiquetees += 1
                        empaquetage.bouteilles += 1
                        requete.etiquetage()
                    except simpy.Interrupt:                        
                        print(f'{self.name} en panne pendant etiquetage, réparateur en route...')
                        with maintenance_agent.request(priority=1) as req:
                            yield req
                            yield self.env.timeout(REPAIR_TIME)
                        self.en_panne = False
                        print(f'{self.name} réparée !!! at {self.env.now}')
            else:
                if self.bouteille_ne == 0:
                    print(' en manque de bouteilles à étiqueter...')
                    try:
                        yield self.env.timeout(5)
                        
                    except simpy.Interrupt:
                        print(f'{self.name} en panne pendant attente étiqueteuse, réparateur en route...')
                        
                        with maintenance_agent.request(priority=1) as req:
                            yield req
                            yield self.env.timeout(REPAIR_TIME)
                        self.en_panne = False
                        print(f'{self.name} réparée !!! at {self.env.now}')

                if self.etiquettes == 0:
                    stock_etiquette_suffisant, qty = requete.transfert_stock_etiquette()
                    
                    if stock_etiquette_suffisant:
                        print('en manque étiquettes ....')
                        recharge_time = 10
                        while recharge_time:
                            try:
                                with maintenance_agent.request(priority=2) as req:
                                    yield req                        
                                    yield self.env.timeout(recharge_time)
                                    self.etiquettes += qty   
                                    print(f'etiquette rechargée {self.etiquettes}')                       
                                    recharge_time = 0
                            except simpy.Interrupt:
                                if self.en_panne:
                                    with maintenance_agent.request(priority=1) as req:
                                        yield req
                                        yield self.env.timeout(REPAIR_TIME)
                                    self.en_panne = False
                                    print(f'Etiqueteuse réparée !!! at {self.env.now}')
                    else:
                        print('il faut commander des etiquettes!!!!!')
                        try:
                            yield self.env.timeout(10)
                        except simpy.Interrupt:
                            if self.en_panne:                                 
                                with maintenance_agent.request(priority=1) as req:
                                    yield req
                                    yield self.env.timeout(REPAIR_TIME)
                                self.en_panne = False
                                print(f'Etiqueteuse réparée !!! at {self.env.now}')

    def panne(self):
        """la machine peut tomber en panne..."""
        while True:
            if self.en_panne:
                yield self.env.timeout(10)
            else:
                time_fail = time_to_failure()
                print(f'Etiqueteuse : en panne dans {2*time_fail}')
                yield self.env.timeout(2*int(time_fail))
                if not self.en_panne:
                    self.en_panne = True  
                    self.pannes += 1                 
                    self.action.interrupt()

class Empaquetage(object):
    def __init__(self,env,name,maintenance_agent):
        self.env = env
        self.name = name
        self.bouteilles = 0
        self.action = env.process(self.run(maintenance_agent))
        self.en_panne = False
        self.pannes = 0
        self.cartons = 0
        #env.process(self.panne())

    def run(self, maintenance_agent):
        while True:
            if self.bouteilles >= 6:
                process_duration = 12
                while process_duration:
                    try:
                        empaqu_start = self.env.now
                        yield self.env.timeout(process_duration)
                        process_duration = 0
                        self.cartons += 1
                        self.bouteilles -= 6
                        requete.empaquetage()
                    except simpy.Interrupt:
                        process_duration -= (self.env.now - empaqu_start)
                        with maintenance_agent.request(priority=1) as req:
                            yield req
                            yield self.env.timeout(REPAIR_TIME)
                        self.en_panne = False
                        print(f'Empaqueuteuse réparée !!! at {self.env.now}') 
            else:
                empaqu_wait = 5
                while empaqu_wait:
                    try:
                        start_wait_empaqu = self.env.now
                        print(f'en attente bouteille sur empaqueteuse, bouteilles : {self.bouteilles}')
                        yield self.env.timeout(12)
                        empaqu_wait = 0
                    except simpy.Interrupt:
                        empaqu_wait -= (self.env.now - start_wait_empaqu)
                        with maintenance_agent.request(priority=1) as req:
                            yield req
                            yield self.env.timeout(REPAIR_TIME)
                        self.en_panne = False
                        print(f'Empaqueuteuse réparée !!! at {self.env.now}')
    def panne(self):
        """la machine peut tomber en panne..."""
        while True:
            if self.en_panne:
                yield self.env.timeout(10)
            else:
                time_fail = time_to_failure()
                print(f'Empaqueteuse : en panne dans {2*time_fail}')
                yield self.env.timeout(2*int(time_fail))
                if not self.en_panne:
                    self.en_panne = True  
                    self.pannes += 1                 
                    self.action.interrupt()

            
                        
requete.reset()

# env = BrodartChampagneEnvironnement()
env = simpy.Environment()
maintenance_agent = simpy.PreemptiveResource(env, capacity=NUM_AGENT)
# env = simpy.RealtimeEnvironment()
remplissage = Remplissage(env, maintenance_agent)
bouchages = [Encapsulage(env,f'Encap_{i+1}',maintenance_agent) for i in range(NUM_ENCAP)]
etiquetages = [Etiquetage(env,f'Etiqu_{i+1}',maintenance_agent) for i in range(NUM_ETIQUE)]
empaquetage = Empaquetage(env,f'Empaqueteuse',maintenance_agent)
env.run(until=TIME)
print('--------------------------------------------------------------------')
print(f'remplissage: pannes {remplissage.pannes}')

total_encap = 0
for bouch in bouchages:
    total_encap += bouch.stock_bouteille_bouchee
    print(
                f"stock {bouch.name}={bouch.stock_bouteille},{bouch.stock_bouchon},{bouch.stock_bouteille_bouchee}, pannes = {bouch.pannes}, envoi = {bouch.envoi}"
            )
print(f'total des bouteilles bouchées : {total_encap}')
for etique in etiquetages:
    print(f'stock {etique.name}: bouteille non etiquetée = {etique.bouteille_ne},bouteilles etiquetées = {etique.bouteille_etiquetees}, étiquette = {etique.etiquettes}, pannes = {etique.pannes}')

print(f'cartons remplis : {empaquetage.cartons}, pannes = {empaquetage.pannes}, bouteilles : {empaquetage.bouteilles}')


