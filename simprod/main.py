#!/usr/bin/env python
import simpy
import random

REPAIR_TIME = 80
NUM_ENCAP = 4
CONF = {"capacite_bouteille": 0.75}
MEAN_PANNE = 150
TIME = 500
NUM_AGENT = 3

def min_stocks_bouteille():
    stocks = [bouchage.stock_bouteille for bouchage in bouchages]
    ind_min = stocks.index(min(stocks))
    return ind_min

def time_to_failure():
    """Return time until next failure for a machine."""
    return random.expovariate(1/MEAN_PANNE)



class Remplissage(object):
    def __init__(self, env):
        self.env = env
        # Start the run process everytime an instance is created.
        self.action = env.process(self.run())
        self.stock_bouteille_vide = 20
        self.stock_vin = 20
        self.stock_bouteille_pleine = 0

    def run(self):
        while True:
            print("Remplissage démarré %d" % self.env.now)
            process_duration = 3
            # We yield the process that process() returns
            # to wait for it to finish
            yield self.env.process(self.remplir(process_duration))

            # The charge process has finished and
            # we can start driving again.
            print("Attente bouteille suivante at %d" % self.env.now)
            trip_duration = 1
            yield self.env.timeout(trip_duration)

    def remplir(self, duration):
        self.stock_bouteille_vide -= 1
        self.stock_vin -= CONF["capacite_bouteille"]
        yield self.env.timeout(duration)
        # self.stock_bouteille_pleine += 1
        # bouchage.stock_bouteille += 1
        bouch = bouchages[min_stocks_bouteille()]
        bouch.stock_bouteille += 1
        print("bouteilles remplies:",bouch.name)
        print(
            f"Bouteille remplie (stock={self.stock_bouteille_vide},{self.stock_vin},{self.stock_bouteille_pleine})"
        )


class Encapsulage(object):
    def __init__(self, env, name, maintenance_agent):
        self.env = env
        # Start the run process everytime an instance is created.
        self.name = name
        self.stock_bouteille = 0
        self.stock_bouchon = 0
        self.stock_bouteille_bouchee = 0
        self.action = env.process(self.run(maintenance_agent))
        self.en_attente = False
        self.en_panne = False
        self.recharge_time = 0
        self.start_recharge = 0
        
        env.process(self.panne())

    def run(self,maintenance_agent):
        while True:
            if self.stock_bouteille > 0 and self.stock_bouchon > 0:
            
                print(
                f"stock {self.name}={self.stock_bouteille},{self.stock_bouchon},{self.stock_bouteille_bouchee}"
            )
                print(f"{self.name}: Encapsulage démarré %d" % self.env.now)
                process_duration = 8
                while process_duration:
                    # We yield the process that process() returns
                    # to wait for it to finish
                    try:
                        start_encap = self.env.now
                        yield self.env.process(self.bouchage(process_duration))
                        process_duration = 0
                    except simpy.Interrupt: 
                        if self.en_panne:                        
                            print(f'{self.name} en panne pendant bouchage, réparateur en route...')
                            process_duration -= self.env.now - start_encap

                            with maintenance_agent.request(priority=1) as req:
                                yield req
                                yield self.env.timeout(REPAIR_TIME)
                            self.en_panne = False
                            print(f'{self.name} réparée !!!')
                        


                # The charge process has finished and
                # we can start driving again.
                print(f"{self.name}:Attente bouteille suivante at %d" % self.env.now)
                trip_duration = 2
                while trip_duration:
                    try:
                        start = self.env.now
                        yield self.env.timeout(trip_duration)
                        trip_duration = 0
                    except simpy.Interrupt:                        
                        print(f'{self.name} en panne pendant attente, réparateur en route...')
                        trip_duration -= self.env.now - start
                        with maintenance_agent.request(priority=1) as req:
                            yield req
                            yield self.env.timeout(REPAIR_TIME)
                        self.en_panne = False
                        print(f'{self.name} réparée !!! at {self.env.now}')
                        
                        

                    
            else:
                if self.stock_bouchon == 0:
                    self.recharge_time = 4
                    while self.recharge_time:
                        try:
                            print(f'{self.name}: en attente bouchons')
                            self.start_recharge = self.env.now
                            print(f' start at {self.start_recharge}', '1')
                            print(f'during {self.recharge_time}', '1')                            
                            with maintenance_agent.request(priority=2) as req:
                                yield req                        
                                yield self.env.timeout(self.recharge_time)
                                self.stock_bouchon += 4                          
                                self.recharge_time = 0  
                                                                            
                        except simpy.Interrupt:
                            if self.en_panne:
                                print(f'{self.name} en panne pendant recharge bouchons, réparateur en route...{self.env.now}')
                                print(f' start at {self.start_recharge}', '1')
                                print(f'during {self.recharge_time}', '1')  
                                #self.recharge_time -= (self.env.now - self.start_recharge)
                                self.recharge_time = 4  
                                print(f'{self.name}: ',self.recharge_time)
                                with maintenance_agent.request(priority=1) as req:
                                    yield req
                                    yield self.env.timeout(REPAIR_TIME)
                                self.en_panne = False
                                print(f'{self.name} réparée !!! at {self.env.now}')
                            else:
                                print(f'{self.name}: une tache plus importante est requise')
                                # self.recharge_time -= (self.env.now - self.start_recharge)
                                self.recharge_time = 4
                
                if self.stock_bouteille == 0:
                    wait_time = 3
                    while wait_time:
                        try:
                            print(f'{self.name}: en attente bouteille')
                            start_wait = self.env.now
                            yield self.env.timeout(wait_time)
                            wait_time = 0  # Set to 0 to exit while loop.
                        except simpy.Interrupt:
                            print(f'{self.name} en panne pendant bouteille vide, réparateur en route...')
                            wait_time -= self.env.now - start_wait
                            with maintenance_agent.request(priority=1) as req:
                                yield req
                                yield self.env.timeout(REPAIR_TIME)
                            self.en_panne = False
                            print(f'{self.name} réparée !!!')
                            

            
                
                            
                        

    def panne(self):
        """la machine peut tomber en panne..."""
        while True:
            if self.en_panne:
                yield self.env.timeout(10)
            else:
                time_fail = time_to_failure()
                print(f'{self.name} : en panne dans {time_fail}')
                yield self.env.timeout(int(time_fail))
                if not self.en_panne:
                    self.en_panne = True                
                    # Only break the machine if it is currently working.
                    #if not self.en_attente:
                    self.action.interrupt()
                
            

    def bouchage(self, duration):
        if self.stock_bouteille > 0 and self.stock_bouchon > 0:
            
            yield self.env.timeout(duration)
            self.stock_bouteille_bouchee += 1
            self.stock_bouteille -= 1
            self.stock_bouchon -= 1
            print(
                f"{self.name}: Bouteille bouchée (stock={self.stock_bouteille},{self.stock_bouchon},{self.stock_bouteille_bouchee} at {self.env.now})"
            )
        else:
            print(f"{self.name}: echec bouchage !!")


# env = BrodartChampagneEnvironnement()
env = simpy.Environment()
maintenance_agent = simpy.PreemptiveResource(env, capacity=NUM_AGENT)
# env = simpy.RealtimeEnvironment()
remplissage = Remplissage(env)
bouchages = [Encapsulage(env,f'Encap_{i+1}',maintenance_agent) for i in range(NUM_ENCAP)]
env.run(until=TIME)

for bouch in bouchages:
    print(
                f"stock {bouch.name}={bouch.stock_bouteille},{bouch.stock_bouchon},{bouch.stock_bouteille_bouchee}"
            )

