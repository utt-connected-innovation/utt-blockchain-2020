## procédure pour le lancement du simulateur 
* installer un environnement virtuel python (à faire uniquement la première fois):
```
python3 -m venv env
```
* lancement de l'environnement virtuel
```
source env/bin/activate
```
* installation des dépendances (à faire uniquement la première fois):
```
pip install simpy
pip install requests
```
* s'assurer que API_ENDPOINT = 'http://api.utt.phec.net' dans les deux fichiers suivants:
    * simprod/requete.py
    * app/activity.py

* aller sur l'url suivante: 
http://api.utt.phec.net/synoptic/index.html

* taper la ligne de commande suivante:
### sans dolibarr
```
python -m simprod.main3
```

### avec dolibarr
```
python -m simprod.main4
```
## envoi de commandes (dans l'environnement virtuel)
* installation de jupyter notebook (à faire uniquement la première fois):
```
pip install jupyter
```
* lancement de jupyter notebook:
```
jupyter notebook
```
* lancer le notebook intitulé envoi_commande.ipynb
* le groupe de Françoise est le groupe 3 dans le notebook
* penser à changer la variable receiver_email que les étudiants doivent créer et fournir


## démarrage de l'environnement Binder:

en navigation privée:

http://binder.utt.phec.net/v2/gl/utt-connected-innovation%2Futt-blockchain-2020/master


