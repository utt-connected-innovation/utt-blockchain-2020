#!/usr/bin/env python
import simpy
import random
# python -m simprod.main3
from app.activity import publish_event
import time
from datetime import datetime
import simprod.requete as requete


try:
    requete.reset()
except:
    pass
'''
Version du simulateur avec la liaison dolibarr
'''
requete.etat_stocks()
now = datetime.now()
date_time = now.strftime("%d/%m/%Y")
date = date_time


STOCKS = requete.etat_stocks()

QTYS = {
    'bouteille_vide': {'id':'1','1': 0,'2': 0, '3': 0},
    'Vin': {'id':'8','1': 0,'2': 0, '3': 0, 'lot':''},
    'bouchon': {'id':'2','1': 0,'2': 0, '3': 0},
    'etiquette': {'id':'3','1': 0,'2': 0, '3': 0},
    'bouteille_remplie': {'id':'4','1': 0,'2': 0, '3': 0},
    'bouteille_bouchée': {'id':'5','1': 0,'2': 0, '3': 0},
    'bouteille_étiquetée': {'id':'6','1': 0,'2': 0, '3': 0},
    'carton': {'id':'7','1': 0,'2': 0, '3': 0}
    }

CARTONS_STOCK3 = STOCKS.get('carton',0).get('3',0)
STOCK_1_VIN = STOCKS.get('Vin',0).get('1',0)
STOCK_1_BOUT = STOCKS.get('bouteille_vide',0).get('1',0)
STOCK_1_BOUCH = STOCKS.get('bouchon',0).get('1',0)
STOCK_1_ETIQU = STOCKS.get('etiquette',0).get('1',0)



REFS = STOCKS.copy()

publish_event('debug_msg', 'Hello' + str(time.time()))
publish_event('stock_1_vin', STOCK_1_VIN)
publish_event('stock_1_etiquette', STOCK_1_ETIQU)
publish_event('stock_1_bouteille_vide', STOCK_1_BOUT)
publish_event('stock_1_bouchon', STOCK_1_BOUCH)
publish_event('date', date)
publish_event('empaqueteuse', 'green')
publish_event('remplisseuse', 'green')
publish_event('etiqueteuse', 'green')
publish_event('Encap_1_panne', 'green')
publish_event('Encap_2_panne', 'green')
publish_event('Encap_3_panne', 'green')
publish_event('stock_3_carton6', CARTONS_STOCK3)


time.sleep(3)

def print_ws(msg):
    print(msg)
    #publish_event('debug_msg', msg)


REPAIR_TIME = 20
NUM_ENCAP = 3
ENCAP_TIME = 10
NUM_ETIQUE = 1
ETIQUE_TIME = 4
CONF = {"capacite_bouteille": 0.75}
MEAN_PANNE = 8000
TIME = 20000
NUM_AGENT = 3
LOT_VIN = ''

def min_stocks_bouteille():
    """
    détermine vers quelle encapsuleuse envoyer la bouteille sortant
    de la remplisseuse
    """
    stocks = [bouchage.stock_bouteille for bouchage in bouchages]
    ind_min = stocks.index(min(stocks))
    return ind_min

def min_stocks_bouteille_bouch():
    """
    détermine vers quelle étiqueteuse envoyer la bouteille sortant
    de l'encapsuleuse
    """
    stocks = [etique.bouteille_ne for etique in etiquetages]
    ind_min = stocks.index(min(stocks))
    return ind_min

def time_to_failure():
    """Return time until next failure for a machine."""
    return random.expovariate(1/MEAN_PANNE)

def check_empty():
    print(remplissage.stock_vin)
    print([ bouch.stock_bouteille for bouch in bouchages])
    print(etiquetages[0].bouteille_ne)
    print(empaquetage.bouteilles)
    print(empaquetage.cartons)
    if remplissage.stock_vin == 0 and [ bouch.stock_bouteille for bouch in bouchages] == [0,0,0] and etiquetages[0].bouteille_ne == 0 and empaquetage.bouteilles <= 5 and empaquetage.cartons == 0:
        empaquetage.bouteilles = 0
        res = True
    else:
        res = False
    return res

class Remplissage(object):
    def __init__(self, env, maintenance_agent):
        
        self.env = env        
        self.bouteille_remplie = 0
        self.action = env.process(self.run(maintenance_agent))
        self.stock_bouteille_vide = 0
        self.stock_vin = 0
        self.stock_bouteille_pleine = 0
        self.en_panne = False
        self.en_attente = False
        self.pannes = 0
        self.pub_bouteilles = False
        self.pub_vin = False
        env.process(self.panne())

    def run(self, maintenance_agent):
        global STOCK_1_VIN
        global STOCK_1_BOUT
        global LOT_VIN
        while True:
            if self.stock_bouteille_vide > 0  and self.stock_vin > 0:
                print_ws(f"Remplissage démarré at {self.env.now}")
                process_duration = 3
                
                try:
                    yield self.env.process(self.remplir(process_duration))
                
                    print_ws(f"Attente bouteille suivante at {self.env.now}" )
                    trip_duration = 1
                    yield self.env.timeout(trip_duration)
                except simpy.Interrupt:
                    if self.en_panne:
                        with maintenance_agent.request(priority=1) as req:
                            yield req
                            yield self.env.timeout(REPAIR_TIME)
                        self.en_panne = False
                        publish_event('remplisseuse', 'green')
                        print_ws(f'Remplissage réparée !!! at {self.env.now}')

            else:
                if self.stock_bouteille_vide == 0:
                    
                    print_ws('en attente bouteilles vides sur remplissage...')
                    publish_event('remplisseuse', 'blue')
                    recharge_time = 10
                    self.pub_bouteilles = False
                    
                    while recharge_time:
                        if STOCK_1_BOUT >0:
                            try:
                                with maintenance_agent.request(priority=2) as req:
                                    if STOCK_1_BOUT >= 24:
                                        if not self.pub_bouteilles:                                        
                                            STOCK_1_BOUT -= 24
                                            publish_event('stock_1_bouteille_vide', STOCK_1_BOUT)
                                            self.pub_bouteilles = True
                                        yield req                        
                                        yield self.env.timeout(recharge_time)
                                        self.stock_bouteille_vide += 24
                                        publish_event('bout_vide_remp', self.stock_bouteille_vide)                                                         
                                        recharge_time = 0
                                        publish_event('remplisseuse', 'green')
                                    elif STOCK_1_BOUT > 0:
                                        if not self.pub_bouteilles:
                                            a = STOCK_1_BOUT
                                            STOCK_1_BOUT = 0
                                            publish_event('stock_1_bouteille_vide', STOCK_1_BOUT)
                                            self.pub_bouteilles = True
                                        yield req                        
                                        yield self.env.timeout(recharge_time)
                                        self.stock_bouteille_vide += a
                                        publish_event('bout_vide_remp', self.stock_bouteille_vide)                                                         
                                        recharge_time = 0
                                        publish_event('remplisseuse', 'green')
                                    
                            except simpy.Interrupt:
                                if self.en_panne:
                                    recharge_time = 4  
                                    
                                    with maintenance_agent.request(priority=1) as req:
                                        yield req
                                        yield self.env.timeout(REPAIR_TIME)
                                    self.en_panne = False
                                    publish_event('remplisseuse', 'green')
                                    print_ws(f'Remplissage réparée !!! at {self.env.now}')
                        else:
                            self.en_attente = True
                            yield self.env.timeout(20)
                            self.en_attente = False

                if self.stock_vin == 0:  
                    print_ws('en attente vin sur remplissage...')
                    publish_event('remplisseuse', 'blue')
                    recharge_time = 10
                    self.pub_vin = False
                    
                    while recharge_time:
                        if STOCK_1_VIN >0:
                            LOTS_VIN = requete.lots_vin()
                            if LOTS_VIN.get(LOT_VIN,0) == 0:
                                cles = list(LOTS_VIN.keys())
                                ind = 0
                                cle = cles[ind]
                                while LOTS_VIN[cle] == 0 and ind < len(cles)-1:
                                    ind += 1
                                    cle = cles[ind]
                                LOT_VIN = cle
                                
                                wait = True
                                while wait:
                                    if not check_empty():
                                        yield self.env.timeout(30)
                                    else:
                                        wait = False
                                        yield self.env.timeout(32)
                                QTYS['Vin']['lot'] = cle


                            try:
                                with maintenance_agent.request(priority=2) as req:
                                    if STOCK_1_VIN >= 30:
                                        if not self.pub_vin:
                                            STOCK_1_VIN -= 30
                                            publish_event('stock_1_vin', STOCK_1_VIN)
                                            self.pub_vin = True
                                        yield req                        
                                        yield self.env.timeout(recharge_time)
                                        self.stock_vin += 30  
                                        publish_event('vin_remp', self.stock_vin)                        
                                                        
                                        recharge_time = 0
                                        publish_event('remplisseuse', 'green')
                                    elif STOCK_1_VIN > 0:
                                        if not self.pub_vin:
                                            a = STOCK_1_VIN
                                            STOCK_1_VIN = 0
                                            publish_event('stock_1_bouteille_vide', STOCK_1_VIN)
                                            self.pub_bouteilles = True
                                        yield req                        
                                        yield self.env.timeout(recharge_time)
                                        self.stock_vin += a
                                        publish_event('bout_vide_remp', self.stock_vin)                                                         
                                        recharge_time = 0
                                        publish_event('remplisseuse', 'green')
                            except simpy.Interrupt:
                                if self.en_panne:
                                    recharge_time = 4  
                                    with maintenance_agent.request(priority=1) as req:
                                        yield req
                                        yield self.env.timeout(REPAIR_TIME)
                                    self.en_panne = False
                                    print_ws(f'Remplissage réparée !!! at {self.env.now}')
                                    publish_event('remplisseuse', 'green')
                        else:
                            self.en_attente = True
                            yield self.env.timeout(20)
                            self.en_attente = False

    def panne(self):
        """la machine peut tomber en panne..."""
        while True:
            if self.en_panne:
                yield self.env.timeout(10)
            else:
                time_fail = time_to_failure()
                yield self.env.timeout(2*int(time_fail))
                if not self.en_panne and not self.en_attente:
                    self.en_panne = True 
                    # import pdb; pdb.set_trace()
                    publish_event('remplisseuse', 'red') 
                    self.pannes += 1                 
                    self.action.interrupt()


    def remplir(self, duration):
        self.bouteille_remplie += 1
        self.stock_bouteille_vide -= 1
        self.stock_vin -= CONF["capacite_bouteille"]
        publish_event('vin_remp', self.stock_vin)
        publish_event('bout_vide_remp', self.stock_bouteille_vide) 
        yield self.env.timeout(duration)
        bouch = bouchages[min_stocks_bouteille()]
        bouch.stock_bouteille += 1
        publish_event(f'{bouch.name}_bout_remp', bouch.stock_bouteille) 
        print_ws(
            f"Bouteille remplie (stock={self.stock_bouteille_vide},{self.stock_vin},{self.stock_bouteille_pleine})"
        )
        

class Monitor(object):
    def __init__(self, env):
        self.env = env        
        env.process(self.run())
        env.process(self.check_stocks())

    def run(self):
        while True:
            yield self.env.timeout(2)
            old_maintenance_agent_solde = None
            maintenance_agent_solde = maintenance_agent.capacity - maintenance_agent.count
            if old_maintenance_agent_solde != maintenance_agent_solde:
                # publish_event('debug_msg2', f"{maintenance_agent.count}, {maintenance_agent.capacity}")
                publish_event('maintenance_agent_solde', maintenance_agent_solde)
                publish_event('maintenance_agent_capacity', maintenance_agent.capacity)
                old_maintenance_agent_solde = maintenance_agent_solde
    
    def check_stocks(self):
        global STOCK_1_BOUCH
        global STOCK_1_BOUT
        global STOCK_1_ETIQU
        global STOCK_1_VIN
        global CARTONS_STOCK3
        global REFS
        global QTYS
        while True:
            yield self.env.timeout(20) 
                  
            
            QTYS['bouteille_vide']['1'] = REFS['bouteille_vide'].get('1') - STOCK_1_BOUT
            QTYS['Vin']['1'] = REFS['Vin'].get('1',0) - STOCK_1_VIN
            QTYS['Vin']['2'] = REFS['Vin'].get('2',0) - remplissage.stock_vin
            QTYS['bouchon']['1'] = REFS['bouchon'].get('1') - STOCK_1_BOUCH
            QTYS['bouchon']['2'] = REFS['bouchon'].get('2',0) - sum([encap.stock_bouchon for encap in bouchages])
            QTYS['etiquette']['1'] = REFS['etiquette'].get('1',0) - STOCK_1_ETIQU
            QTYS['etiquette']['2'] = REFS['etiquette'].get('2',0) - etiquetages[0].etiquettes
            QTYS['bouteille_remplie']['2'] = REFS['bouteille_remplie'].get('2',0) - sum([encap.stock_bouteille for encap in bouchages]) 
            QTYS['bouteille_bouchée']['2'] = REFS['bouteille_bouchée'].get('2',0) - etiquetages[0].bouteille_ne
            QTYS['bouteille_étiquetée']['2'] = REFS['bouteille_étiquetée'].get('2',0) - empaquetage.bouteilles
            QTYS['bouteille_vide']['2'] = REFS['bouteille_vide'].get('2') - remplissage.stock_bouteille_vide
            QTYS['carton']['2'] = REFS['carton'].get('2',0) - empaquetage.cartons
            QTYS['carton']['3'] = REFS['carton'].get('3',0) - CARTONS_STOCK3
            '''
            for idx,qty in enumerate(qtys):
                if qty != 0:
                    if idx < 3:
                        requete.destock(idx + 1, qty)
                    elif 3 <= idx  <= 5:
                        requete.destock(idx+1, -qty)                        
                    else:
                        requete.destock(idx+1, qty)
            '''
            requete.destocks_lots(QTYS)             
            #yield self.env.timeout(2) 
            STOCKS = requete.etat_stocks()
            #yield self.env.timeout(1)        
            STOCK_1_VIN = STOCKS.get('Vin',0).get('1',0)
            STOCK_1_BOUT = STOCKS.get('bouteille_vide',0).get('1',0)
            STOCK_1_BOUCH = STOCKS.get('bouchon',0).get('1',0)
            STOCK_1_ETIQU = STOCKS.get('etiquette',0).get('1',0)
            CARTONS_STOCK3 = STOCKS.get('carton',0).get('3',0)
            REFS = STOCKS.copy()
            publish_event('stock_1_vin', STOCK_1_VIN)
            publish_event('stock_1_etiquette', STOCK_1_ETIQU)
            publish_event('stock_1_bouteille_vide', STOCK_1_BOUT)
            publish_event('stock_1_bouchon', STOCK_1_BOUCH)
            publish_event('stock_3_carton6', CARTONS_STOCK3)




class Encapsulage(object):
    def __init__(self, env, name, maintenance_agent):
        self.env = env        
        self.name = name
        self.stock_bouteille = 0
        self.stock_bouchon = 0
        self.stock_bouteille_bouchee = 0
        self.action = env.process(self.run(maintenance_agent))
        self.en_attente = False
        self.en_panne = False
        self.recharge_time = 0
        self.start_recharge = 0
        self.pannes = 0
        self.envoi = 0
        self.bouchee = False
        self.pub_bouchons = False
        self.en_attente = False
       
        
        
        env.process(self.panne())

    def run(self,maintenance_agent):
        global STOCK_1_BOUCH
        while True:
            
            if self.stock_bouteille > 0 and self.stock_bouchon > 0:
            
                print_ws(
                f"stock {self.name}={self.stock_bouteille},{self.stock_bouchon},{self.stock_bouteille_bouchee}"
            )
                print_ws(f"{self.name}: Encapsulage démarré at {self.env.now}")
                process_duration = ENCAP_TIME
                self.bouchee = False
                envoyee = False
                while not envoyee:
                    
                    try:
                        if not self.bouchee:                        
                            start_encap = self.env.now                            
                            yield self.env.timeout(process_duration)
                            self.stock_bouteille_bouchee += 1                            
                            self.stock_bouteille -= 1
                            publish_event(f'{self.name}_bout_remp', self.stock_bouteille)
                            self.stock_bouchon -= 1
                            publish_event(f'{self.name}_bouchon', self.stock_bouchon)
                            self.bouchee = True
                            print_ws(f"{self.name}:Attente bouteille suivante at %d" % self.env.now)
                            print_ws(f"{self.name}: Envoi vers étiqueteuse")
                            yield self.env.timeout(2)
                            min_etiqu = etiquetages[min_stocks_bouteille_bouch()]
                            min_etiqu.bouteille_ne += 1
                            publish_event(f'bout_ne',min_etiqu.bouteille_ne )
                            self.envoi += 1
                            
                            process_duration = 0
                            envoyee = True
                        else:
                            print_ws(f"{self.name}:Attente bouteille suivante at {self.env.now}")
                            print_ws(f"{self.name}: Envoi vers étiqueteuse")
                            yield self.env.timeout(2)
                            min_etiqu = etiquetages[min_stocks_bouteille_bouch()]
                            min_etiqu.bouteille_ne += 1
                            publish_event(f'bout_ne',min_etiqu.bouteille_ne )
                            self.envoi += 1
                            process_duration = 0 
                            envoyee = True
                        
                    except simpy.Interrupt: 
                        if self.en_panne:                                                   
                            print_ws(f'{self.name} en panne pendant bouchage, réparateur en route...')
                            process_duration -= int(self.env.now - start_encap)

                            with maintenance_agent.request(priority=1) as req:
                                yield req
                                yield self.env.timeout(REPAIR_TIME)
                            self.en_panne = False
                            publish_event(f'{self.name}_panne', 'green')
                            print_ws(f'{self.name} réparée !!!')
                        else:
                            print_ws('<<<<<<<<<<<<<<<<<<<<<<<<<<autre pb>>>>>>>>>>>>>>>>>>>')
                   
            else:
                if self.stock_bouchon == 0:
                    publish_event(f'{self.name}_panne', 'blue')
                    
                    
                    self.recharge_time = 4
                    self.pub_bouchons = False
                    
                    
                    while self.recharge_time:
                        if STOCK_1_BOUCH >0:
                            try:
                                print_ws(f'{self.name}: en attente bouchons')                                
                                self.start_recharge = self.env.now
                                #print_ws(f' start at {self.start_recharge}', '1')
                                #print_ws(f'during {self.recharge_time}', '1')                            
                                with maintenance_agent.request(priority=2) as req:
                                    if STOCK_1_BOUCH >= 40:
                                        if not self.pub_bouchons:
                                            STOCK_1_BOUCH -= 40   
                                            publish_event('stock_1_bouchon', STOCK_1_BOUCH)
                                            self.pub_bouchons = True
                                        yield req                        
                                        yield self.env.timeout(self.recharge_time)
                                        self.stock_bouchon += 40
                                        publish_event(f'{self.name}_bouchon', self.stock_bouchon)                                                
                                        self.recharge_time = 0
                                        publish_event(f'{self.name}_panne', 'green')  
                                    elif STOCK_1_BOUCH > 0:
                                        if not self.pub_bouchons:
                                            a = STOCK_1_BOUCH                                            
                                            STOCK_1_BOUCH = 0   
                                            publish_event('stock_1_bouchon', STOCK_1_BOUCH)
                                            self.pub_bouchons = True
                                        yield req                        
                                        yield self.env.timeout(self.recharge_time)
                                        self.stock_bouchon += a
                                        publish_event(f'{self.name}_bouchon', self.stock_bouchon)                                                
                                        self.recharge_time = 0
                                        publish_event(f'{self.name}_panne', 'green')

                                                                                
                            except simpy.Interrupt:
                                if self.en_panne:
                                    print_ws(
                                        f'{self.name} en panne pendant recharge bouchons, réparateur en route...{self.env.now}'
                                        )
                                                    
                                    self.recharge_time = 4  
                                    print_ws(f'{self.name}: ',self.recharge_time)
                                    with maintenance_agent.request(priority=1) as req:
                                        yield req
                                        yield self.env.timeout(REPAIR_TIME)
                                    self.en_panne = False
                                    publish_event(f'{self.name}_panne', 'green')
                                    print_ws(f'{self.name} réparée !!! at {self.env.now}')
                                else:
                                    print_ws(f'{self.name}: une tache plus importante est requise')
                                    
                                    self.recharge_time = 4
                        else:
                            self.en_attente = True
                            yield self.env.timeout(20)
                            self.en_attente = False
                    

                if self.stock_bouteille == 0:
                    wait_time = 3
                    
                    while wait_time:
                        try:
                            print_ws(f'{self.name}: en attente bouteille')
                            publish_event(f'{self.name}_attente_bouteille', 1)
                            publish_event(f'attente_bouteille', self.name)
                            start_wait = self.env.now
                            yield self.env.timeout(wait_time)
                            wait_time = 0  # Set to 0 to exit while loop.
                        except simpy.Interrupt:
                            print_ws(f'{self.name} en panne pendant bouteille vide, réparateur en route...')
                            wait_time -= self.env.now - start_wait
                            with maintenance_agent.request(priority=1) as req:
                                yield req
                                yield self.env.timeout(REPAIR_TIME)
                            self.en_panne = False
                            publish_event(f'{self.name}_panne', 'green')
                            print_ws(f'{self.name} réparée !!!')        
                
                            
                        

    def panne(self):
        """la machine peut tomber en panne..."""
        while True:
            if self.en_panne:
                yield self.env.timeout(10)
            else:
                time_fail = time_to_failure()
                print_ws(f'{self.name} : en panne dans {time_fail}')
                yield self.env.timeout(int(time_fail))
                if not self.en_panne and not self.en_attente:
                    self.en_panne = True 
                    publish_event(f'{self.name}_panne', 'red') 
                    self.pannes += 1                   
                    self.action.interrupt()
                
            

    def bouchage(self, duration):
        if self.stock_bouteille > 0 and self.stock_bouchon > 0:
            
            
            
            print_ws(
                f"{self.name}: Bouteille bouchée (stock={self.stock_bouteille},{self.stock_bouchon},{self.stock_bouteille_bouchee} at {self.env.now})"
            )
        else:
            print_ws(f"{self.name}: echec bouchage !!")

class Etiquetage(object):
    def __init__(self,env,name,maintenance_agent):
        
        self.env = env
        self.name = name
        self.bouteille_ne = 0
        self.etiquettes = 0
        self.bouteille_e = 0
        self.action = env.process(self.run(maintenance_agent))
        self.en_panne = False
        self.pannes = 0
        self.bouteille_etiquetees = 0
        self.pub = False
        self.pub_bout = False
        self.en_attente = False
        env.process(self.panne())
        

    def run(self, maintenance_agent):
        global STOCK_1_ETIQU
        while True:
            if self.bouteille_ne > 0 and self.etiquettes > 0:
                print_ws(f'{self.name}: Etiquetage démarré at {self.env.now}')
                etique_duration = ETIQUE_TIME
                while etique_duration:
                    try:
                        start_etiqu = self.env.now
                        yield self.env.timeout(etique_duration)                        
                        self.etiquettes -= 1
                        publish_event('etiquette_etiqueteuse', self.etiquettes)
                        self.bouteille_ne -= 1
                        publish_event('bout_ne', self.bouteille_ne)
                        self.bouteille_etiquetees += 1
                        publish_event('bout_e', self.bouteille_etiquetees)
                        empaquetage.bouteilles += 1
                        publish_event('Empaq_bouteilles', empaquetage.bouteilles)
                        etique_duration = 0
                        
                    except simpy.Interrupt:                        
                        print_ws(f'{self.name} en panne pendant etiquetage, réparateur en route...')
                        with maintenance_agent.request(priority=1) as req:
                            yield req
                            yield self.env.timeout(REPAIR_TIME)
                        self.en_panne = False
                        publish_event('etiqueteuse', 'green')
                        print_ws(f'{self.name} réparée !!! at {self.env.now}')
            else:
                if self.bouteille_ne == 0:
                    print_ws(' en manque de bouteilles à étiqueter...')
                    try:
                        yield self.env.timeout(5)
                        
                    except simpy.Interrupt:
                        print_ws(f'{self.name} en panne pendant attente étiqueteuse, réparateur en route...')
                        
                        with maintenance_agent.request(priority=1) as req:
                            yield req
                            yield self.env.timeout(REPAIR_TIME)
                        self.en_panne = False
                        publish_event('etiqueteuse', 'green')
                        print_ws(f'{self.name} réparée !!! at {self.env.now}')

                if self.etiquettes == 0:
                    publish_event('etiqueteuse', 'blue')    
                    print_ws('en manque étiquettes ....')
                    recharge_time = 10
                    self.pub = False
                    
                    while recharge_time:
                        if STOCK_1_ETIQU > 0:
                            try:
                                with maintenance_agent.request(priority=2) as req:
                                    if STOCK_1_ETIQU > 100:
                                        if not self.pub:
                                            STOCK_1_ETIQU -= 100
                                            publish_event('stock_1_etiquette', STOCK_1_ETIQU)
                                            self.pub = True
                                        yield req                        
                                        yield self.env.timeout(recharge_time)
                                        self.etiquettes += 100                                
                                        publish_event('etiquette_etiqueteuse', self.etiquettes)
                                        print_ws(f'etiquette rechargée {self.etiquettes}')                       
                                        recharge_time = 0
                                        publish_event('etiqueteuse', 'green')
                                    elif STOCK_1_ETIQU > 0:
                                        if not self.pub:
                                            a = STOCK_1_ETIQU
                                            STOCK_1_ETIQU = 0
                                            publish_event('stock_1_etiquette', STOCK_1_ETIQU)
                                            self.pub = True
                                        yield req                        
                                        yield self.env.timeout(recharge_time)
                                        self.etiquettes += a                                
                                        publish_event('etiquette_etiqueteuse', self.etiquettes)
                                        print_ws(f'etiquette rechargée {self.etiquettes}')                       
                                        recharge_time = 0
                                        publish_event('etiqueteuse', 'green')

                            except simpy.Interrupt:
                                if self.en_panne:
                                    with maintenance_agent.request(priority=1) as req:
                                        yield req
                                        yield self.env.timeout(REPAIR_TIME)
                                    self.en_panne = False
                                    print_ws(f'Etiqueteuse réparée !!! at {self.env.now}')
                                    publish_event('etiqueteuse', 'green')
                        else:
                            self.en_attente = True
                            yield self.env.timeout(20)
                            self.en_attente = False
                

    def panne(self):
        """la machine peut tomber en panne..."""
        while True:
            if self.en_panne:
                yield self.env.timeout(10)
            else:
                time_fail = time_to_failure()
                print_ws(f'Etiqueteuse : en panne dans {2*time_fail}')
                yield self.env.timeout(2*int(time_fail))
                if not self.en_panne and not self.en_attente:
                    publish_event('etiqueteuse', 'red')
                    self.en_panne = True  
                    self.pannes += 1                 
                    self.action.interrupt()

class Empaquetage(object):
    def __init__(self,env,name,maintenance_agent):
        
        self.env = env
        self.name = name
        self.bouteilles = 0
        self.action = env.process(self.run(maintenance_agent))
        self.en_panne = False
        self.pannes = 0
        self.cartons = 0
        self.pub_bouteilles = False
        self.pub_cartons = False
        self.en_envoi = False
        env.process(self.panne())
        env.process(self.envoi_stock3())

    def run(self, maintenance_agent):
        while True:
            if self.bouteilles >= 6:
                process_duration = 12
                while process_duration:
                    try:
                        empaqu_start = self.env.now
                        yield self.env.timeout(process_duration)
                        process_duration = 0
                        self.cartons += 1
                        publish_event('cartons', self.cartons)
                                                
                        self.bouteilles -= 6
                        publish_event('Empaq_bouteilles', self.bouteilles)
                        
                    except simpy.Interrupt:
                        process_duration -= (self.env.now - empaqu_start)
                        with maintenance_agent.request(priority=1) as req:
                            yield req
                            yield self.env.timeout(REPAIR_TIME)
                        self.en_panne = False
                        publish_event('remplisseuse', 'green')
                        print_ws(f'Empaqueuteuse réparée !!! at {self.env.now}') 
            else:
                empaqu_wait = 5
                while empaqu_wait:
                    try:
                        start_wait_empaqu = self.env.now
                        print_ws(f'en attente bouteille sur empaqueteuse, bouteilles : {self.bouteilles}')
                        yield self.env.timeout(12)
                        empaqu_wait = 0
                    except simpy.Interrupt:
                        empaqu_wait -= (self.env.now - start_wait_empaqu)
                        with maintenance_agent.request(priority=1) as req:
                            yield req
                            yield self.env.timeout(REPAIR_TIME)
                        self.en_panne = False
                        print_ws(f'Empaqueuteuse réparée !!! at {self.env.now}')
                        publish_event('empaqueteuse', 'green')
    def panne(self):
        """la machine peut tomber en panne..."""
        while True:
            if self.en_panne:
                yield self.env.timeout(10)
            else:
                time_fail = time_to_failure()
                print_ws(f'Empaqueteuse : en panne dans {2*time_fail}')
                yield self.env.timeout(2*int(time_fail))
                if not self.en_panne:
                    self.en_panne = True  
                    publish_event('empaqueteuse', 'red')
                    self.pannes += 1                 
                    self.action.interrupt()

    def envoi_stock3(self):
        global CARTONS_STOCK3  
        while True:
            if self.en_envoi:
                yield self.env.timeout(30)
            else:
                if self.cartons >= 1:
                    try:
                        with maintenance_agent.request(priority=2) as req:
                            yield req
                            yield self.env.timeout(10)
                            self.cartons -= 1
                            publish_event('cartons', self.cartons)
                            yield self.env.timeout(10)
                            CARTONS_STOCK3 += 1
                            publish_event('stock_3_carton6', CARTONS_STOCK3)
                    except simpy.Interrupt:
                        yield self.env.timeout(30)
                else:
                    yield self.env.timeout(30)





                       
                        


# env = BrodartChampagneEnvironnement()
#env = simpy.Environment()
env = simpy.RealtimeEnvironment(strict=False, factor = 0.5)
monitor = Monitor(env)
maintenance_agent = simpy.PreemptiveResource(env, capacity=NUM_AGENT)
# env = simpy.RealtimeEnvironment()
remplissage = Remplissage(env, maintenance_agent)
bouchages = [Encapsulage(env,f'Encap_{i+1}',maintenance_agent) for i in range(NUM_ENCAP)]
etiquetages = [Etiquetage(env,f'Etiqu_{i+1}',maintenance_agent) for i in range(NUM_ETIQUE)]
empaquetage = Empaquetage(env,f'Empaqueteuse',maintenance_agent)

env.run(until=TIME)
print_ws('--------------------------------------------------------------------')
print_ws(f'remplissage: pannes {remplissage.pannes}')

total_encap = 0
for bouch in bouchages:
    total_encap += bouch.stock_bouteille_bouchee
    print_ws(
                f"stock {bouch.name}={bouch.stock_bouteille},{bouch.stock_bouchon},{bouch.stock_bouteille_bouchee}, pannes = {bouch.pannes}, envoi = {bouch.envoi}"
            )
print_ws(f'total des bouteilles bouchées : {total_encap}')
for etique in etiquetages:
    print_ws(f'stock {etique.name}: bouteille non etiquetée = {etique.bouteille_ne},bouteilles etiquetées = {etique.bouteille_etiquetees}, étiquette = {etique.etiquettes}, pannes = {etique.pannes}')

print_ws(f'cartons remplis : {empaquetage.cartons}, pannes = {empaquetage.pannes}, bouteilles : {empaquetage.bouteilles}')

