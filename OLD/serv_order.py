from flask import Flask, render_template, request, jsonify, url_for
import requests
import json
import datetime
import random as rd



HEADERS = {'DOLAPIKEY': 'JRy6EoG0uSVOl1D432cbx682bTkibTB2'}


app = Flask(__name__)

@app.route('/commande')
def commande():
    return render_template('commande.html')

@app.route('/order/send', methods = ['POST'])
def send_order():
    cartons = request.json['nombre_cartons']
    data = {
        "socid": "4",
        "type": "0",
        "date": int(datetime.datetime.now().timestamp()),
        'lines': [{
            "desc": "",
            "product_ref": "0014",
            "subprice": "150.00",
            "qty": cartons,
            "tva_tx": "20.00",
            "fk_product": "4"
        }
        ],
        "note_private": "Commande"
    }
    data = json.dumps(data)
    post_comm = requests.post('http://localhost:8080/api/index.php/orders', headers= HEADERS, data = data)
    order_id = post_comm.text
    validate = requests.post(f'http://localhost:8080/api/index.php/orders/{order_id}/validate', headers= HEADERS)
    return jsonify('bien')

@app.route('/lancer/production')
def production():
    return render_template('production.html')

@app.route('/details/stock')
def detail_stock():
    req = requests.get('http://localhost:8080/api/index.php/products?sortfield=t.ref&sortorder=ASC&limit=100000', headers = HEADERS)
    labels_id = {}
    for lab in req.json():
        labels_id[lab['label']] = lab['id']
    
    req = requests.get('http://localhost:8080/api/index.php/stockmovements?sortorder=ASC&limit=100000', headers = HEADERS)
    warehouses = []
    for lab in req.json():
        if lab['warehouse_id'] not in warehouses:
            warehouses.append(lab['warehouse_id'])
    stocks = {}
    for key in labels_id:
        stocks[key] = {}
        for house in warehouses:
            stocks[key][house] = 0        
            for lab in req.json():
                if lab['product_id'] == labels_id[key] and lab['warehouse_id'] == house:
                    stocks[key][house] += float(lab['qty'])
    print(stocks)
    return json.dumps(stocks)

def production(product_id, qty):    
    data = {
        "product_id": product_id,
        "warehouse_id": 2,
        "qty": qty,
        "lot": "",
        "movementcode": "INV123",
        "movementlabel": "Inventory 123",
     }
    req = requests.post('http://localhost:8080/api/index.php/stockmovements', headers = HEADERS, data = data)

@app.route('/production', methods=['POST'])
def lancement_production():
    prod_qty = request.json
    print(prod_qty)
    incident = rd.randint(1,10)
    if incident != 10:
        res = 'aucun incident'
        for id in prod_qty:
            production(id, prod_qty[id])
    else:
        req = requests.get('http://localhost:8080/api/index.php/products?sortfield=t.ref&sortorder=ASC&limit=100000', headers = HEADERS)
        labels_id = {}
        for lab in req.json():
            labels_id[lab['id']] = lab['label']
        
        a = rd.choice(list(prod_qty.keys()))
        res = f'problème sur {labels_id[a]}'

    return jsonify(res)




if __name__ == '__main__':
    app.run(
        host = '0.0.0.0',
        port = 5000,
        debug=True
    )