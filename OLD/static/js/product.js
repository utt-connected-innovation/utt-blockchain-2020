var nv = new Vue({
    el: '#elem',
    delimiters: ['[[', ']]'],
    
    data : {
        stocks : {},
        message : ''
    },
    methods : {
        stocks_stat : function(event) {
                       
            self = this;
            axios.get('/details/stock',
            
            {headers: {'Accept': 'application/json'}}).then(res=>{
                self.stocks = res.data;                
                       
                
              })
            console.log(self.stocks)

            
        },
        modalprod : function(){
                
            $("#modprod").modal('show');
            
        },

        prod : function() {
            self = this;
            var bout = this.stocks['bouteille_verre']['2'];
            var bouch = this.stocks['bouchon']['2'];
            var rai = this.stocks['raisin_pressé']['2'];
            var etiq = this.stocks['etiquette']['2'];
            var cart = $('#cart_prod')[0].value;
            if ( 6 * cart > bout ){
                alert('nombre de bouteilles insuffisant!!')
            } else if (6 * cart > bouch) {
                alert('nombre de bouchons insuffisant!!')
            } else if (6 * cart > etiq) {
                alert("nombre d'étiquettes insuffisant!!")
            } else if (6 * 0.75 * cart > rai) {
                alert("raisin insuffisant!!")
            } else {
                console.log('tout va bien')
                axios.post('/production',
                    {
                        '1' : - 6 * cart,
                        '2' : - 6 * 0.75 * cart,
                        '3' : - 6 * cart,
                        '4' : cart,
                        '5' : - 6 * cart
                    },
                    {headers: {'Accept': 'application/json'}}).then(res=>{
                    self.message = res.data;
                    self.stocks_stat();                
                       
                
              })

            }

        }
    },
    mounted : function(){
        self = this;
        self.stocks_stat();
        
    }, 

    

    
})