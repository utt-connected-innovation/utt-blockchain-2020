from m5stack import *
from m5ui import *
from uiflow import *
import unit
import urequests
import network

version = 10
spot_color = 120 << 16
# url = 'http://192.168.200.71:8000/temp'
url = "http://api.utt.phec.net/temp"
wifi_ap = "phecAP"
wifi_password = "gelannes"

setScreenColor(0x222222)
label0 = M5TextBox(131, 94, "Text", lcd.FONT_Default, 0xFFFFFF, rotate=0)
label0.setText("init v" + str(version) + " ...")


label0.setText("init angle ...")
angle0 = unit.get(unit.ANGLE, unit.PORTB)
label0.setText("neopixel ...")
neopixel0 = unit.get(unit.NEOPIXEL, unit.PORTA, 40)
# rfid0 = unit.get(unit.RFID, unit.PORTA)


label0.setText("init gui ...")
circle0 = M5Circle(240, 33, 15, 0xFFFFFF, 0xFFFFFF)
rectangle0 = M5Rect(33, 39, 30, 80, 0xC31111, 0xFFFFFF)
rectangle1 = M5Rect(46, 104, 30, 30, 0xFFFFFF, 0xFFFFFF)
label1 = M5TextBox(20, 200, "v" + str(version), lcd.FONT_Default, 0xFFFFFF, rotate=90)

# Connextion au réseau WiFI (utile en usage sans webIDE)
n = 1
label0.setText("init wifi pre1 ...")
sta_if = network.WLAN(network.STA_IF)
wait(1)
label0.setText("init wifi pre2 ...")
sta_if.active(True)
wait(1)
label0.setText("init wifi pre3 ...")
sta_if.connect(wifi_ap, wifi_password)
while not sta_if.isconnected():
    label0.setText("init wifi " + str(n) + "...")
    n = n + 1
    wait(1)

while True:
    try:
        v = angle0.read()
        c = int(v / 4)
        label0.setText(str(v) + ", " + str(c))
        neopixel0.setColorFrom(1, 40, c)
        for i in range(1, 40):
            neopixel0.setColorFrom(i - 2, i, spot_color)
            if i > 3:
                neopixel0.setColorFrom(i - 3, i - 3, c)
            wait(0.01)

        req = urequests.post(url, json={"temp": str(v)})
        t = req.json()
    except:
        pass
    wait(1)

