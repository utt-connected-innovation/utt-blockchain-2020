from m5stack import *
from m5ui import *
from uiflow import *
import unit

setScreenColor(0x111111)
neopixel0 = unit.get(unit.NEOPIXEL, unit.PORTA, 37)




circle0 = M5Circle(95, 90, 15, 0xFFFFFF, 0xFFFFFF)
circle1 = M5Circle(221, 90, 15, 0xFFFFFF, 0xFFFFFF)

import random

R = None
G = None
B = None
i = None



while True:
  R = random.randint(0, 255)
  G = random.randint(0, 255) 
  B = random.randint(0, 255) 

  C = (R << 16) | (G << 8) | B
  # C = B + G * 256 + R * 256 * 256
  

  for i in range(1, 38):
    neopixel0.setColorFrom(1,i, C)
    # wait(0.001)
  wait_ms(400)
  for i in range(1, 38):
    neopixel0.setColorFrom(1, i, 0x000000)
    # wait(0.005)
