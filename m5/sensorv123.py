from m5stack import *
from m5ui import *
import units

clear_bg(0x000000)
env0 = units.ENV(units.PORTA)



label0 = M5TextBox(117, 104, "T :", lcd.FONT_Default,0xFFFFFF, rotate=0)
label1 = M5TextBox(117, 145, "P :", lcd.FONT_Default,0xFFFFFF, rotate=0)
label2 = M5TextBox(117, 185, "H :", lcd.FONT_Default,0xFFFFFF, rotate=0)
label3 = M5TextBox(157, 104, "Text", lcd.FONT_Default,0xffffff, rotate=0)
label4 = M5TextBox(154, 144, "Text", lcd.FONT_Default,0xFFFFFF, rotate=0)
label5 = M5TextBox(154, 184, "Text", lcd.FONT_Default,0xFFFFFF, rotate=0)


import random

random2 = None
i = None



while True:
  try:
    #req = urequests.post(url, json={"temp": str(env0.temperature), "pression": str(env0.pressure), "hum":str(env0.humidity)})
    req = urequests.get(url + '/capteur/hum/' + str(env0.humidity()))
    req = urequests.get(url + '/capteur/temp/' + str(env0.temperature()))
    req = urequests.get(url + '/capteur/pression/' + str(env0.pressure()))
    
    wait(0.1)
  except:
    pass
  label3.setText(str(env0.temperature()))
  label4.setText(str(env0.pressure()))
  label5.setText(str(env0.humidity()))
  
  wait(1)
