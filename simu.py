
import simpy
import random


def simulation(num_encap,num_etiqu,num_agent):
    REPAIR_TIME = 3600
    NUM_ENCAP = num_encap
    ENCAP_TIME = 20
    NUM_ETIQUE = num_etiqu
    ETIQUE_TIME = 5
    CONF = {"capacite_bouteille": 0.75}
    MEAN_PANNE = 3600 * 8 * 5 
    TIME = 60 * 60 * 8 * 5 * 4
    NUM_AGENT = num_agent


    def min_stocks_bouteille():
        """
        détermine vers quelle encapsuleuse envoyer la bouteille sortant
        de la remplisseuse
        """
        stocks = [bouchage.stock_bouteille for bouchage in bouchages]
        ind_min = stocks.index(min(stocks))
        return ind_min

    def min_stocks_bouteille_bouch():
        """
        détermine vers quelle étiqueteuse envoyer la bouteille sortant
        de l'encapsuleuse
        """
        stocks = [etique.bouteille_ne for etique in etiquetages]
        ind_min = stocks.index(min(stocks))
        return ind_min

    def time_to_failure():
        """Return time until next failure for a machine."""
        return random.expovariate(1/MEAN_PANNE)



    class Remplissage(object):
        def __init__(self, env, maintenance_agent):
            self.env = env        
            self.bouteille_remplie = 0
            self.action = env.process(self.run(maintenance_agent))
            self.stock_bouteille_vide = 0
            self.stock_vin = 0
            self.stock_bouteille_pleine = 0
            self.en_panne = False
            self.pannes = 0
            env.process(self.panne())

        def run(self, maintenance_agent):
            while True:
                if self.stock_bouteille_vide > 0  and self.stock_vin > 1:
                    
                    process_duration = 2

                    try:
                        yield self.env.process(self.remplir(process_duration))

                        
                        trip_duration = 1
                        yield self.env.timeout(trip_duration)
                    except simpy.Interrupt:
                        if self.en_panne:
                            with maintenance_agent.request(priority=1) as req:
                                yield req
                                yield self.env.timeout(REPAIR_TIME*random.randint(1,5))
                            self.en_panne = False
                            

                else:
                    if self.stock_bouteille_vide == 0:

                        
                        recharge_time = 10
                        while recharge_time:
                            try:
                                with maintenance_agent.request(priority=2) as req:
                                    yield req                        
                                    yield self.env.timeout(recharge_time)
                                    self.stock_bouteille_vide += 24                        
                                    recharge_time = 0
                            except simpy.Interrupt:
                                if self.en_panne:
                                    recharge_time = 4  

                                    with maintenance_agent.request(priority=1) as req:
                                        yield req
                                        yield self.env.timeout(REPAIR_TIME * random.randint(1,5))
                                    self.en_panne = False
                                    

                    if self.stock_vin <= 1:  
                        
                        recharge_time = 10
                        while recharge_time:
                            try:
                                with maintenance_agent.request(priority=2) as req:
                                    yield req                        
                                    yield self.env.timeout(recharge_time)
                                    self.stock_vin += 30                          
                                    recharge_time = 0
                            except simpy.Interrupt:
                                if self.en_panne:
                                    recharge_time = 4  
                                    with maintenance_agent.request(priority=1) as req:
                                        yield req
                                        yield self.env.timeout(REPAIR_TIME * random.randint(1,5))
                                    self.en_panne = False
                                    

        def panne(self):
            """la machine peut tomber en panne..."""
            while True:
                if self.en_panne:
                    yield self.env.timeout(10)
                else:
                    time_fail = time_to_failure()
                    yield self.env.timeout(2*int(time_fail))
                    if not self.en_panne:
                        self.en_panne = True  
                        self.pannes += 1                 
                        self.action.interrupt()


        def remplir(self, duration):
            self.bouteille_remplie += 1
            self.stock_bouteille_vide -= 1
            self.stock_vin -= CONF["capacite_bouteille"]
            yield self.env.timeout(duration)
            bouch = bouchages[min_stocks_bouteille()]
            bouch.stock_bouteille += 1
            



    class Encapsulage(object):
        def __init__(self, env, name, maintenance_agent):
            self.env = env

            self.name = name
            self.stock_bouteille = 0
            self.stock_bouchon = 0
            self.stock_bouteille_bouchee = 0
            self.action = env.process(self.run(maintenance_agent))
            self.en_attente = False
            self.en_panne = False
            self.recharge_time = 0
            self.start_recharge = 0
            self.pannes = 0
            self.envoi = 0
            self.bouchee = False


            env.process(self.panne())

        def run(self,maintenance_agent):
            while True:

                if self.stock_bouteille > 0 and self.stock_bouchon > 0:

                    
                    process_duration = ENCAP_TIME
                    self.bouchee = False
                    envoyee = False
                    while not envoyee:

                        try:
                            if not self.bouchee:                        
                                start_encap = self.env.now                            
                                yield self.env.timeout(process_duration)
                                self.stock_bouteille_bouchee += 1                            
                                self.stock_bouteille -= 1
                                self.stock_bouchon -= 1
                                self.bouchee = True
                                
                                yield self.env.timeout(2)
                                etiquetages[min_stocks_bouteille_bouch()].bouteille_ne += 1
                                self.envoi += 1

                                process_duration = 0
                                envoyee = True
                            else:
                                
                                yield self.env.timeout(2)
                                etiquetages[min_stocks_bouteille_bouch()].bouteille_ne += 1
                                self.envoi += 1
                                process_duration = 0 
                                envoyee = True

                        except simpy.Interrupt: 
                            if self.en_panne:                                                   
                                
                                process_duration -= int(self.env.now - start_encap)

                                with maintenance_agent.request(priority=1) as req:
                                    yield req
                                    yield self.env.timeout(REPAIR_TIME * random.randint(1,5))
                                self.en_panne = False
                                
                            else:
                                print('<<<<<<<<<<<<<<<<<<<<<<<<<<autre pb>>>>>>>>>>>>>>>>>>>')

                else:
                    if self.stock_bouchon == 0:


                        self.recharge_time = 4
                        while self.recharge_time:
                            try:
                                                                
                                self.start_recharge = self.env.now
                                #print(f' start at {self.start_recharge}', '1')
                                #print(f'during {self.recharge_time}', '1')                            
                                with maintenance_agent.request(priority=2) as req:
                                    yield req                        
                                    yield self.env.timeout(self.recharge_time)
                                    self.stock_bouchon += 40

                                    self.recharge_time = 0  

                            except simpy.Interrupt:
                                if self.en_panne:
                                    

                                    self.recharge_time = 4  
                                    
                                    with maintenance_agent.request(priority=1) as req:
                                        yield req
                                        yield self.env.timeout(REPAIR_TIME * random.randint(1,5))
                                    self.en_panne = False
                                    
                                else:
                                    print(f'{self.name}: une tache plus importante est requise')

                                    self.recharge_time = 4


                    if self.stock_bouteille == 0:
                        wait_time = 3
                        while wait_time:
                            try:
                                
                                start_wait = self.env.now
                                yield self.env.timeout(wait_time)
                                wait_time = 0  # Set to 0 to exit while loop.
                            except simpy.Interrupt:
                                
                                wait_time -= self.env.now - start_wait
                                with maintenance_agent.request(priority=1) as req:
                                    yield req
                                    yield self.env.timeout(REPAIR_TIME * random.randint(1,5))
                                self.en_panne = False
                                      




        def panne(self):
            """la machine peut tomber en panne..."""
            while True:
                if self.en_panne:
                    yield self.env.timeout(10)
                else:
                    time_fail = time_to_failure()
                    
                    yield self.env.timeout(int(time_fail))
                    if not self.en_panne:
                        self.en_panne = True  
                        self.pannes += 1                   
                        self.action.interrupt()



        def bouchage(self, duration):
            if self.stock_bouteille > 0 and self.stock_bouchon > 0:



                print(
                    f"{self.name}: Bouteille bouchée (stock={self.stock_bouteille},{self.stock_bouchon},{self.stock_bouteille_bouchee} at {self.env.now})"
                )
            else:
                print(f"{self.name}: echec bouchage !!")

    class Etiquetage(object):
        def __init__(self,env,name,maintenance_agent):
            self.env = env
            self.name = name
            self.bouteille_ne = 0
            self.etiquettes = 0
            self.bouteille_e = 0
            self.action = env.process(self.run(maintenance_agent))
            self.en_panne = False
            self.pannes = 0
            self.bouteille_etiquetees = 0
            env.process(self.panne())


        def run(self, maintenance_agent):
            while True:
                if self.bouteille_ne > 0 and self.etiquettes > 0:
                    
                    etique_duration = ETIQUE_TIME
                    while etique_duration:
                        try:
                            start_etiqu = self.env.now
                            yield self.env.timeout(etique_duration)                        
                            self.etiquettes -= 1
                            self.bouteille_ne -= 1
                            self.bouteille_etiquetees += 1
                            empaquetage.bouteilles += 1
                            etique_duration = 0

                        except simpy.Interrupt:                        
                            
                            with maintenance_agent.request(priority=1) as req:
                                yield req
                                yield self.env.timeout(REPAIR_TIME * random.randint(1,5))
                            self.en_panne = False
                            
                else:
                    if self.bouteille_ne == 0:
                        
                        try:
                            yield self.env.timeout(5)

                        except simpy.Interrupt:
                            

                            with maintenance_agent.request(priority=1) as req:
                                yield req
                                yield self.env.timeout(REPAIR_TIME * random.randint(1,5))
                            self.en_panne = False
                            

                    if self.etiquettes == 0:



                        
                        recharge_time = 10
                        while recharge_time:
                            try:
                                with maintenance_agent.request(priority=2) as req:
                                    yield req                        
                                    yield self.env.timeout(recharge_time)
                                    self.etiquettes += 100   
                                                           
                                    recharge_time = 0
                            except simpy.Interrupt:
                                if self.en_panne:
                                    with maintenance_agent.request(priority=1) as req:
                                        yield req
                                        yield self.env.timeout(REPAIR_TIME * random.randint(1,5))
                                    self.en_panne = False
                                    


        def panne(self):
            """la machine peut tomber en panne..."""
            while True:
                if self.en_panne:
                    yield self.env.timeout(10)
                else:
                    time_fail = time_to_failure()
                    
                    yield self.env.timeout(2*int(time_fail))
                    if not self.en_panne:
                        self.en_panne = True  
                        self.pannes += 1                 
                        self.action.interrupt()

    class Empaquetage(object):
        def __init__(self,env,name,maintenance_agent):
            self.env = env
            self.name = name
            self.bouteilles = 0
            self.action = env.process(self.run(maintenance_agent))
            self.en_panne = False
            self.pannes = 0
            self.cartons = 0
            env.process(self.panne())

        def run(self, maintenance_agent):
            while True:
                if self.bouteilles >= 6:
                    process_duration = 12
                    while process_duration:
                        try:
                            empaqu_start = self.env.now
                            yield self.env.timeout(process_duration)
                            process_duration = 0
                            self.cartons += 1
                            self.bouteilles -= 6

                        except simpy.Interrupt:
                            process_duration -= (self.env.now - empaqu_start)
                            with maintenance_agent.request(priority=1) as req:
                                yield req
                                yield self.env.timeout(REPAIR_TIME)
                            self.en_panne = False
                            
                else:
                    empaqu_wait = 5
                    while empaqu_wait:
                        try:
                            start_wait_empaqu = self.env.now
                            
                            yield self.env.timeout(12)
                            empaqu_wait = 0
                        except simpy.Interrupt:
                            empaqu_wait -= (self.env.now - start_wait_empaqu)
                            with maintenance_agent.request(priority=1) as req:
                                yield req
                                yield self.env.timeout(REPAIR_TIME * random.randint(1,5))
                            self.en_panne = False
                            
        def panne(self):
            """la machine peut tomber en panne..."""
            while True:
                if self.en_panne:
                    yield self.env.timeout(10)
                else:
                    time_fail = time_to_failure()
                    
                    yield self.env.timeout(2*int(time_fail))
                    if not self.en_panne:
                        self.en_panne = True  
                        self.pannes += 1                 
                        self.action.interrupt()





    # env = BrodartChampagneEnvironnement()
    env = simpy.Environment()
    maintenance_agent = simpy.PreemptiveResource(env, capacity=NUM_AGENT)
    # env = simpy.RealtimeEnvironment()
    remplissage = Remplissage(env, maintenance_agent)
    bouchages = [Encapsulage(env,f'Encap_{i+1}',maintenance_agent) for i in range(NUM_ENCAP)]
    etiquetages = [Etiquetage(env,f'Etiqu_{i+1}',maintenance_agent) for i in range(NUM_ETIQUE)]
    empaquetage = Empaquetage(env,f'Empaqueteuse',maintenance_agent)
    env.run(until=TIME)

    total_encap = 0
    for bouch in bouchages:
        total_encap += bouch.stock_bouteille_bouchee
    cartons = empaquetage.cartons
    cout = num_agent * 2500
    print(f'nombre de cartons : {cartons}')
    print(f'cout : {cout}')
    
    
    return cartons,cout

def multi_simu(num_encap,num_etiqu,num_agent,nbr_simu):
    X = [i+1 for i in range(nbr_simu)]
    Y = []
    for i in range(nbr_simu):
        (y1,y2) = simulation(num_encap,num_etiqu,num_agent)
        Y.append(y1)
    return X, Y
    
    

