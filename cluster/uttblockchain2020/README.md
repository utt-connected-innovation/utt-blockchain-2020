# Installation de l'environnement

## ERP et Synoptique

Construction de l'image API

```bash
docker-compose build api
docker-compose push api
```

Installation via Helm :

```bash
cd cluster/uttblockchain2020
helm upgrade --install uttblockchain2020 . \
    --namespace=utt \
    --values values.yaml \
    --set services.api.domain="api.utt.phec.net",services.erp.domain="erp.utt.phec.net",services.erp.password="PASSWORD"
```

Obtention de les 2 IPs externes (patientez 5 min):

```bash
kubectl get svc -n utt
```

Modification du DNS : **api.utt.phec.net** 1800 IN CNAME **EXTERNAL-IP** et **erp.utt.phec.net** 1800 IN CNAME **EXTERNAL-IP**

## Environnement de travail

cf ph.ec/binder/binderhub

- Notebook : http://binder.utt.phec.net/v2/gl/utt-connected-innovation%2Futt-blockchain-2020/master

http://binder.utt.phec.net/v2/gl/utt-connected-innovation%2Futt-blockchain-2020/0c91c0125710d03150ca5d833b56157edf2df408

# Accès à l'application

- ERP : http://erp.utt.phec.net : login _admin_ / _PASSWORD_

- API : http://api.utt.phec.net/docs/

- Synoptique : http://api.utt.phec.net/synoptic/index.html

Pour tester :

- http://api.utt.phec.net/synoptic/index.html

- http://api.utt.phec.net/docs#/default/publish_publish_post :

```json
{
  "key": "stock_1_etiquette",
  "value": "10"
}
```

# Remettre l'ERP dans une version X

```bash
kubectl get pods -n utt|grep erp
# dsrv-erp-5b47b86dcb-mkv5x

export POD_NAME="dsrv-erp-5b47b86dcb-mkv5x"
export CHECKPOINT="etat1"

kubectl cp cluster/dolibarr_checkpoints/$CHECKPOINT/documents.tar.gz utt/$POD_NAME:/tmp/documents.tar.gz

kubectl cp cluster/dolibarr_checkpoints/$CHECKPOINT/mysqldump.sql.gz utt/$POD_NAME:/tmp/mysqldump.sql.gz

kubectl exec -it $POD_NAME -n utt -- sh
  bash

  cd /tmp

  gunzip mysqldump.sql.gz
  /usr/bin/mysql erp -h mariadb -P 3306 -u root -ppwd2020pwd < mysqldump.sql

  tar -xvzf documents.tar.gz
  cd /var/www/documents/
  rm -r admin  api  commande  doctemplates  expedition  facture  fournisseur product  societe  stock  users
  cp -r /tmp/documents/* .

  rm -r /tmp/documents /tmp/documents.tar.gz /tmp/mysqldump.sql

  exit
  exit
```
