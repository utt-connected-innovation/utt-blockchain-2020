import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);


interface State {
  // last message key:value
  last_message: string;
  // last messages [key:value, ...]
  last_messages: string;
  // last message key
  last_message_key: string;
  // last message value
  last_message_value: string;
  // last message datetime
  last_message_date: Date;
  nb_message: number;
  // Mapping of current values
  messages: { [id: string]: string; };
}


const state0: State = {
  last_message: '',
  last_messages: '',
  last_message_key: '',
  last_message_value: '',
  last_message_date: new Date(),
  nb_message: 0,
  messages: {},
};

export default new Vuex.Store({
  state: state0,
  mutations: {
    recieve(state: any, message: string) {
      state.last_message = message;
      state.last_message_date = new Date();

      const items = message.split(':');
      const key = items.shift() || '';
      state.last_message_key = key;
      const value = items.join(':');​​​​​
      state.last_message_value = value;
      state.nb_message = state.nb_message + 1;

      // state[key] = value;
      Vue.set(state, key, value);

      // state.messages[key] = value;
      const obj2: { [id: string]: string; } = {};
      obj2[key] = value;
      state.messages = {...state.messages, ...obj2};
    },
  },
  actions: {

  },
  getters: {
    last_message: (state) => state.last_message,
    getMessageByKey: (state) => (key: string) => state.messages[key],
  },
});
