import anime from 'animejs';

export function translate(element: any) {
  anime({
    targets: element,
    translateX: 500,
  });
}
