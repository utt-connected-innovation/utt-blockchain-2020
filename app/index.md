# Synoptique

websocket avec FastAPI

```
uvicorn synoptic.main:app --reload 
```

à voir :
* https://github.com/MetinSeylan/Vue-Socket.io

## Animation

cf [animejs](https://animejs.com/)

with vuejs : https://stackoverflow.com/questions/49451759/how-to-use-anime-js-or-any-external-library-in-a-vue-cli-project

Exemples d'animation :
- https://codepen.io/andreystarkov/pen/MJBvXw
- https://codepen.io/manoj1991/pen/RjdXjN
- https://codepen.io/Yasio/pen/wEWyLE

alternatives :

- [svg.js](https://github.com/svgdotjs/svg.js)


```
anime({
  targets: '#rect6333',
  width: '10',
  duration: 5000
}); 
```