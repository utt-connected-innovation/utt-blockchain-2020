# producer.py

import asyncio
import websockets
import time


async def browser_server(websocket, path):
    print("browser_server starts ...")
    # channel, conn = await subscribe_to_redis(path)
    try:
        while True:
            # Wait until data is published to this channel
            # message = await channel.get()
            message = f"Hello {time.time()}"

            await asyncio.sleep(3)
            # Send unicode decoded data over to the websocket client
            print(time.time(), f"send")
            await websocket.send(message)

    except websockets.exceptions.ConnectionClosed:
        print("ws connexion closed")


if __name__ == "__main__":
    print("starting ...")
    # Runs a server process on 8767. Just do 'python producer.py'
    loop = asyncio.get_event_loop()
    loop.set_debug(True)
    ws_server = websockets.serve(browser_server, "localhost", 8760)
    loop.run_until_complete(ws_server)
    loop.run_forever()
