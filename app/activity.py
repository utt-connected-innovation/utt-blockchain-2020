import time
import requests 
import random


period = 0.3 # seconds
keys = [
        'stock_1_vin',
        'stock_1_bouteille_vide',
        'stock_1_bouchon',
        'stock_1_etiquette',
        'stock_2_bouteille_remplie',
        'stock_2_bouteille_bouchee',
        'stock_2_bouteille_etiquette',
        'stock_3_carton6',
        'chemin_bouteille_remplie',
    ]

headers = {'DOLAPIKEY': 'rLc6LDR94MQ5cMTog80i8ioni7UDSv45'}
URL = 'http://erp.utt.phec.net/api/index.php'

# Mettre la bonne URL pulique ici !!
#API_ENDPOINT = 'http://localhost:8000'
API_ENDPOINT = 'http://api.utt.phec.net'


def publish_event(key: str, value):
    data = {'key': key, 
        'value':value}
    r = requests.post(url = API_ENDPOINT + '/publish', json = data) 


def publish_events(values: dict):
    '''
    values est un dict de clé:valeur
    '''
    r = requests.post(url = API_ENDPOINT + '/publishs', json = values) 


def main():
    nb = 0
    while True:
        nb += 1
        key = random.choice(keys)
        v = random.randint(100, 5000)

        publish_event(key, v)

        print(f'{nb}: publish {key}:{v}')

        time.sleep(period)

def get_stocks():
    req = requests.get(f'{URL}/products?sortfield=t.ref&sortorder=ASC&limit=10000', headers = headers)
    print('la')
    labels_id = {}
    for lab in req.json():
        labels_id[lab['label']] = lab['id']
       
    req = requests.get(f'{URL}/stockmovements?sortorder=ASC&limit=10000', headers = headers)
    warehouses = []
    for lab in req.json():
        if lab['warehouse_id'] not in warehouses:
            warehouses.append(lab['warehouse_id'])

    stocks = {}
    for key in labels_id:
        stocks[key] = {}
        for house in warehouses:
            stocks[key][house] = 0        
            for lab in req.json():
                
                if lab['product_id'] == labels_id[key] and lab['warehouse_id'] == house:
                    stocks[key][house] += float(lab['qty'])
    return stocks

def etat_stock(product_id, warehouse):
    
    req = requests.get(f'{URL}/stockmovements?sortorder=ASC&limit=1000000', headers = headers)
    prod = 0
    try:
        for lab in req.json():
            if lab['product_id'] == product_id and lab['warehouse_id'] == warehouse:
                prod += float(lab['qty'])
    except:
        pass
    return prod

def reset():
    for i in range(8):
        if i == 6:
            qty = etat_stock(f'{i+1}', '3')
            data = {
                "product_id": f'{i+1}',
                "warehouse_id": '3',
                "qty": -qty,
                "lot": "",
                "movementcode": "INV123",
                "movementlabel": "Inventory 123",
                }
            req = requests.post(f'{URL}/stockmovements', headers = headers, data = data)
        
        qty = etat_stock(f'{i+1}', '2')
        data = {
            "product_id": f'{i+1}',
            "warehouse_id": '2',
            "qty": -qty,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
        req = requests.post(f'{URL}/stockmovements', headers = headers, data = data)


def destocks(id, qty):
    if id != 7:
        war = 1
    else:
        war = 3
    data = {
            "product_id": id,
            "warehouse_id": war,
            "qty": -qty,
            "lot": "",
            "movementcode": "INV123",
            "movementlabel": "Inventory 123",
            }
    req = requests.post(
                f'{URL}/stockmovements',
                 headers = headers,
                  data = data
                  )

def destocks2(QTYS):
    '''
    QTYS = {
        'bouteille_vide': {'id':'1','1': 0,'2': 0, '3': 0},
        'Vin': {'id':'8','1': 0,'2': 0, '3': 0},
        'bouchon': {'id':'2','1': 0,'2': 0, '3': 0},
        'etiquette': {'id':'3','1': 0,'2': 0, '3': 0},
        'bouteille_remplie': {'id':'4','1': 0,'2': 0, '3': 0},
        'bouteille_bouchée': {'id':'5','1': 0,'2': 0, '3': 0},
        'bouteille_étiquetée': {'id':'6','1': 0,'2': 0, '3': 0},
        'carton': {'id':'7','1': 0,'2': 0, '3': 0}
    }
    '''
    warhouses = ['1','2','3']
    for qty in QTYS:
        for warhouse in warhouses:
            quant = QTYS[qty][warhouse]
            ide = QTYS[qty]['id']
            if quant != 0:
                '''
                if  ide != '8':
                '''
                data = {
                    "product_id": QTYS[qty]['id'],
                    "warehouse_id": warhouse,
                    "qty": -quant,
                    "lot": "",
                    "movementcode": "INV123",
                    "movementlabel": "Inventory 123",
                }
                req = requests.post(
                    f'{URL}/stockmovements',
                    headers = headers,
                    data = data
                )
                '''
                else:
                    LOTS_VIN = []
                    req_vin = requests.get('http://erp.utt.phec.net/api/index.php/stockmovements?sortfield=t.rowid&sortorder=ASC&limit=100000', headers = headers)
                    for requ in req_vin.json():
                        if requ['type']=='3' and 'Réception' in requ['label'] and requ['product_id']=='8':
                            LOTS_VIN.append(requ['batch'])
                    continuer = True
                    while continuer:
                '''


def destocks2_lots(QTYS):
    '''
    QTYS = {
        'bouteille_vide': {'id':'1','1': 0,'2': 0, '3': 0},
        'Vin': {'id':'8','1': 0,'2': 0, '3': 0, 'lot':''},
        'bouchon': {'id':'2','1': 0,'2': 0, '3': 0},
        'etiquette': {'id':'3','1': 0,'2': 0, '3': 0},
        'bouteille_remplie': {'id':'4','1': 0,'2': 0, '3': 0},
        'bouteille_bouchée': {'id':'5','1': 0,'2': 0, '3': 0},
        'bouteille_étiquetée': {'id':'6','1': 0,'2': 0, '3': 0},
        'carton': {'id':'7','1': 0,'2': 0, '3': 0}
    }
    '''
    warhouses = ['1','2','3']
    for qty in QTYS:
        for warhouse in warhouses:
            quant = QTYS[qty][warhouse]
            ide = QTYS[qty]['id']
            if quant != 0:
                
                if  ide != '8' and ide != '7':
                
                    data = {
                        "product_id": QTYS[qty]['id'],
                        "warehouse_id": warhouse,
                        "qty": -quant,
                        "lot": "",
                        "movementcode": "INV123",
                        "movementlabel": "Inventory 123",
                    }
                    req = requests.post(
                        f'{URL}/stockmovements',
                        headers = headers,
                        data = data
                    )
                elif ide == '8':
                    data = {
                    "product_id": QTYS[qty]['id'],
                    "warehouse_id": warhouse,
                    "qty": -quant,
                    "lot": QTYS['Vin']['lot'],
                    "movementcode": "INV123",
                    "movementlabel": "Inventory 123",
                    }
                    print(QTYS['Vin']['lot'])
                    req = requests.post(
                        f'{URL}/stockmovements',
                        headers = headers,
                        data = data
                    )
                else:
                    data = {
                    "product_id": QTYS[qty]['id'],
                    "warehouse_id": warhouse,
                    "qty": -quant,
                    "lot": QTYS['Vin']['lot'],
                    "movementcode": "INV123",
                    "movementlabel": "Inventory 123",
                    }
                    print(QTYS['Vin']['lot'])
                    req = requests.post(
                        f'{URL}/stockmovements',
                        headers = headers,
                        data = data
                    )

                '''
                else:
                    LOTS_VIN = []
                    req_vin = requests.get('http://erp.utt.phec.net/api/index.php/stockmovements?sortfield=t.rowid&sortorder=ASC&limit=100000', headers = headers)
                    for requ in req_vin.json():
                        if requ['type']=='3' and 'Réception' in requ['label'] and requ['product_id']=='8':
                            LOTS_VIN.append(requ['batch'])
                    continuer = True
                    while continuer:
                '''

if __name__ == "__main__":
    main()