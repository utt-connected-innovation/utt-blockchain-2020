/*
PUBLISH : ws://example.com:8766/toto
LISTEN : ws://example.com:8768/toto
*/
var ws_url = "ws://www.example.com/socketserver";
var ws_url = "ws://localhost:8760/toto";
var exampleSocket = new WebSocket(ws_url);


exampleSocket.onopen = function (event) {
  exampleSocket.send("Voici un texte que le serveur attend de recevoir dès que possible !");
};

exampleSocket.onmessage = function (event) {
  console.log(event.data);
};


exampleSocket.close();