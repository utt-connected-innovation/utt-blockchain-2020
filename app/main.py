import time

import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel
from starlette.responses import HTMLResponse, RedirectResponse
from starlette.staticfiles import StaticFiles
from starlette.websockets import WebSocket, WebSocketDisconnect

import requests
import json


from cryptography.fernet import Fernet
import base64
import urllib3
urllib3.disable_warnings()

#Cors
from starlette.middleware.cors import CORSMiddleware
origins = [
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:8000",
]

from activity import publish_event
from activity import get_stocks
from activity import reset

PAXPAR_URL = 'https://api.paxpar.tech'

app = FastAPI(
    title="Brodard Champagne",
    description="API traçabilité ERP/IoT/blockchain",
    version="1.0.5",
)

#CORS
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.mount("/synoptic", StaticFiles(directory="dist"), name="synoptic")


html = """
<!DOCTYPE html>
<html>
    <head>
        <title>Chat</title>
    </head>
    <body>
        <h1>WebSocket Chat</h1>
        <form action="" onsubmit="sendMessage(event)">
            <input type="text" id="messageText" autocomplete="off"/>
            <button>Send</button>
        </form>
        <ul id='messages'>
        </ul>
        <script>
            let host = "ws://" + window.location.host + "/ws";
            console.log(host)
            var ws = new WebSocket(host);

            ws.onmessage = function(event) {
                var messages = document.getElementById('messages')
                var message = document.createElement('li')
                var content = document.createTextNode(event.data)
                message.appendChild(content)
                messages.appendChild(message)
            };

            function sendMessage(event) {
                var input = document.getElementById("messageText")
                ws.send(input.value)
                input.value = ''
                event.preventDefault()
            }
        </script>
    </body>
</html>
"""


# cheap redis clone ;-)
# wss = []
class CheapPubSub:
    def __init__(self):
        self.histo = []
        self.wss = []

    async def publish(self, message):
        # TODO: move to async loop
        self.histo.append(message)
        print('publishhhhh :', message)
        for ws in self.wss:
            try:
                print(ws)
                await ws.send_text(message)
            except:
                self.unregister(ws)
                self.register(ws)
                print(ws)
                await ws.send_text(message)

    def register(self, ws):
        if ws not in self.wss:
            self.wss.append(ws)
            # for old_msg in self.histo:
            #    print('old_msg',old_msg)
            #    ws.send_text(old_msg)

    def unregister(self, ws):
        print("Disconnecting ws ...")
        self.wss.remove(ws)


pubsub = CheapPubSub()


class Publication(BaseModel):
    key: str
    value: str = None

    def serialize(self):
        return f"{self.key}:{self.value}"


class Publication2(BaseModel):
    temp: str
    pression: str
    hum: str


# TODO: limit storae size !!
storage_limit = 500
print(f"Séries limitées à {storage_limit} points.")
storage = {}


async def store(key, value):
    if key not in storage:
        storage[key] = []
    serie = storage[key]
    serie.append({"t": int(time.time()), "v": float(value)})
    while len(serie) > storage_limit:
        print(f"Série {key} trop longue !")
        serie.pop(0)
    msg = f"{key}:{value}"
    await pubsub.publish(msg)


@app.get('/unlock/{password}', summary="debloque la clé API PAXPAR")
async def unlock(password: str):
    key = base64.encodestring(bytes(password * 8, 'utf8')).strip()
    f = Fernet(key)
    token = b'gAAAAABdoIYthugEDUtvkligoCn5yWp84qhv971urWIO3J8DiLSD1t8gm53VKBBmscXoHns-ST577boT5zlmqcm9acBeweMVCaftS0Z6yqF7MEgauE7EvyTgLY6ZV92uohyzGrKImvQHi3q5dk5qjD0pXHmS7MKHVuDXtwdE9b4dhhN_YcOirND0JKY-ONwEChWiNXJZcjtlDE6GePvUgunGatG4eZo6I3gUx8ZNudDDvexxZKR3-Di8wyPc3j5KbtknEYPG5Ot56605BrqBA4UDaNzY4FQygErIzcuWbnuJjGrmo7WrVgKY1xyeSIP7xHYQvJqJag0K_RT8E5lOtoXpfZOEpcjBjZGuIrjCdF1ZJ2x-SfGjVJJF-_VidaIq2fQJEP6aHx-xKDC6Ckf1VpyQlTq4GD6YIQlhlEua0xHfBBzD9KmV9my8YwyxXnxJLNwnuD-05bM7G-9IOG5zGOUNzItseVpMNYP7-adhsl_qiN-8GFbXQw4CAks9xpkxsHVTsvNg57ksb79Lap2YzhU-bFrwHWfHc9XUYAnoyPD8DOBsoYHFnd-yjMv4js9WDRfnCq-m3L1JRsotVRCkDKD5ARPWAzYpSpIc5o5eaQ3BUdFHMh1EnDlH-r7SEZqJ_93uKPJlAEgWnHw0yZ0ThnHYPQ1WvE6qQT_HXIqx4Jd1x5nOS4Gt-I74QBIoCZgXo0p5xFzEhHmCLQ-8UKj4MfmLhDz1Noy165XEhdmuLDM_O4zrYZ9yczAERVgn6vxHmJ0UVK_f7uKhHdzr3WSb7Z2gfx9ZSHHd0dF0Euu4z5k5rSbkWQnpdFfG2t0OsLj4EMLcl1upPztcCo5viNRglkt51Fim2ciAp3w42aDlnzxgLfVsezH6lRDlkt1p2x7piu3Xc0FgEyfaHczTX8NXYeMICewu67JsXyoG_Negk5y4os9YhTlS6Gs='
    API_KEY = f.decrypt(token).decode('utf-8')
    with open('API_KEY.txt', mode = 'w') as f:
        f.write(API_KEY)

@app.post('/blockchain', summary="envoie les données sur la blockchain")
async def blockchain(datas:list):
    
    with open('API_KEY.txt', mode = 'r') as f:    
        KEY = f.read()
    r = requests.post(
        f"{PAXPAR_URL}/blockchain/transaction",
        json={"data": json.dumps(datas)},
        headers={"Authorization": f"Bearer {KEY}"},
        verify=False,
        )   
    res = r.json()['hash']
    #print(r.json())
                                           
    
    return res



@app.post("/temp", summary="Publie une température", deprecated=True)
@app.post("/temperature", summary="Publie une température")
async def temp(publication: Publication2):
    print("temp: ", publication)
    t = publication.temp
    await store("temp", float(t))
    #await pubsub.publish(f'temp:{t}')


@app.get("/temperature", summary="Dernière température")
async def temp_get():
    raise NotImplementedError


@app.get("/temperatures", summary="Toutes les températures")
async def temps_get():
    raise NotImplementedError


@app.get("/capteur/{capteur}/{valeur}", summary="Publie une valeur")
async def capteur_post(capteur: str, valeur: float):
    if capteur == 'pression':
        valeur = str(round(float(valeur),1))
    msg = f"{valeur}:{valeur}"
    print(f"capteur {msg}")
    await store(capteur, valeur)


@app.get("/capteur/{capteur}", summary="Les valeurs d'un capteur")
async def capteur_get(capteur: str):
    res = storage.get(capteur, [])[-1]
    return res


@app.get("/storage", summary="Toutes les données")
async def storage_get():
    """
    Renvoie toutes les clé avec tous les couples (date, valeur) collectés.
    """
    return storage


@app.post("/publish")
async def publish(publication: Publication):
    print("publish ...", publication)
    await pubsub.publish(publication.serialize())
    return {"ok": True}


@app.get("/")
async def get():
    return HTMLResponse(html)


@app.get("/synoptic2.svg")
async def synoptic2():
    return RedirectResponse(url="/synoptic/synoptic2.svg")


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    pubsub.register(websocket)
    try:
        while True:
            data = await websocket.receive_text()
            # await websocket.send_text(f"Message text was: {data}")
            await pubsub.publish(f"Message text was: {data}")
    except WebSocketDisconnect:
        print("Disconnecting ws ...")
        pubsub.unregister(websocket)

@app.get('/getstocks')
async def getstocks():
    return get_stocks()

from activity import destocks
from fastapi import BackgroundTasks

def dolibarr_api(datas: dict):
    destocks(datas['id'], datas['qty'])

@app.post('/destock')
def destock(datas: dict,  background_tasks: BackgroundTasks):
    print("-------------------->",datas)
    # destocks(datas['id'], datas['qty'])
    background_tasks.add_task(dolibarr_api, datas)    
    return 'done'


from activity import destocks2

def dolibarr_api2(datas: dict):
    destocks2(datas)

@app.post('/destocks')
def destock2(datas: dict,  background_tasks: BackgroundTasks):
    destocks2(datas)
    #background_tasks.add_task(dolibarr_api2, datas)    
    return 'done'

from activity import destocks2_lots

@app.post('/destocks_lots')
def destock2_lots(datas: dict,  background_tasks: BackgroundTasks):
    destocks2_lots(datas)
    #background_tasks.add_task(dolibarr_api2, datas)    
    return 'done'

@app.get('/reset')
def resets():
    return reset()

    



if __name__ == "__main__":
    uvicorn.run(app, port=8000, host="0.0.0.0", debug=True)

